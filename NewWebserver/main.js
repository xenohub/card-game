const express = require('express');
const stringify = require('json-stringify-safe');
const bodyParser = require('body-parser');
const requestIp = require('request-ip');
const fs = require('fs');
const cp = require('child_process');
const expressWs = require('express-ws');
const { Client } = require('pg');
const crypto = require('crypto');
var Convert = require('ansi-to-html');
var convert = new Convert();
const path = require ('path');
const port = process.env.PORT || 80;
const connection = process.env.DATABASE_URL || 'postgres://localhost:5432/database_name';
require('fs').copyFileSync(path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/lobby.exe'), path.resolve(__dirname, 'lobby.exe'));
require('fs').copyFileSync(path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/lobby.pdb'), path.resolve(__dirname, 'lobby.pdb'));
require('fs').copyFileSync(path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/lobby.dll'), path.resolve(__dirname, 'lobby.dll'));
//COLOR CODING: CYAN-PROTOCOL BLUE-SUBPROTOCOL YELLOW-INCOMING GREEN-OUTGOING
function colorMarkup(dat){
	if(dat.indexOf(':') != -1) dat = '\u001b[96m'+dat.substring(0,dat.indexOf(':'))+'\u001b[0m'+dat.substring(dat.indexOf(':'));
	else dat = '\u001b[96m'+dat+'\u001b[0m';
	if(dat.substring(0,dat.indexOf(':')).includes('update')){
		var index = 0;
		while(true){
			if(dat.substring(index).indexOf(':') == -1) break;
			index += dat.substring(index).indexOf(':');
			dat = dat.substring(0,index)+'\u001b[94m'+dat.substring(index, dat.indexOf('!',index))+'\u001b[0m'+dat.substring(dat.indexOf('!',index));
			index += 7;
		}
	}
	return dat;
}

//webhost + childprocesses + websockets
var servers = {}; //non-encoded, BE CAREFUL (encode in future)
var names = {}; //uri component encoded
var IDs = {}; 
var connections = {}; 
var app = express();
/*var wsInstance = */expressWs(app);
app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.text({type:'text/plain'}));
app.use(function(req, res, next) { 
	req.ip = requestIp.getClientIp(req);
	req.cookies = {};
	if(req.headers.cookie !== undefined){
		var match = req.headers.cookie.match(/userID=[\w]{10,10}/g);
		if(match !== null){ 
			req.cookies.userID = match[0].substring(7);
			res.cookie('userID', req.cookies.userID);
		}
	}
	//uri encoded strings will contain -_.!~*'()% and a-z A-Z 0-9 
	if(req.body !== null) req.body = encodeURIComponent(req.body);
	for(var i in req.query){req.query[i] = encodeURIComponent(req.query[i]);}


					//QUERY AND BODY IS ENCODED


	//fs.writeFile('test.json', stringify(req, null, '\t'), (err)=>{if(err) console.error(err);});
	next();
});

app.get('/', function(req, res){ 
	//*
	if(names[req.cookies.userID] !== undefined)
		res.redirect('lobby');
	else {
		res.statusCode = 200; 
		res.render('main'); 
	}/**\/
	res.cookie('userID', '0000000000');
	IDs['CryoFlame'] = '0000000000';
	names['0000000000'] = 'CryoFlame';
	res.statusCode = 200;
	res.render('lobbyRedirect');/**/
});

app.get('/dispose', (res, req) => {});

app.get('/lobby', function(req, res){
	if(names[req.cookies.userID] == undefined)
		res.redirect('..');
	else if(req.query.l == undefined){
		res.statusCode = 200; 
		res.render('lobbyBrowser'); 
	}
	else if(servers[req.query.l] !== undefined && (servers[req.query.l].password == null || servers[req.query.l].authorized.includes(req.cookies.userID))){
		res.statusCode = 200; 
		res.cookie('username', names[req.cookies.userID]);
		res.render('index'); 
	}
	else{
		res.redirect('lobby');
	}
});

app.get('/data', (req, res) => {
	switch(req.query.q){
		case 'servers':
		var dat = {};
		for(var i in servers){
			dat[i] = {};
			dat[i].users = servers[i].users;
			dat[i].open = servers[i].open;
			dat[i].name = servers[i].name;
			dat[i].password = servers[i].password !== null;
			dat[i].connected = servers[i].connected;
		}
		res.status(200).send(JSON.stringify(dat));
		break;
		case 'port':
		res.status(200).send(port.toString());
		break;
		case 'getcolor':
		if(names[req.cookies.userID] !== undefined){
			db.query("SELECT themehue FROM users WHERE username = $1", [names[req.cookies.userID]], (err, ret) =>{
				if(ret.rows.length == 1){
					if(ret.rows[0].themehue !== undefined) res.append('themeHue', ret.rows[0].themehue);
					else res.append('themeHue', '210');
					res.status(200).send('success');
				}
				else res.status(500).send('an error has occured');
			});
		}
		break;
		default:
		res.status(400).send('bad request');
	}
});

app.post('/data', (req, res) =>{
	switch(req.query.q){
		case 'joinserver':
		if(servers[req.body] !== undefined && req.body && (servers[req.body].connected.length < 8 || !servers[req.body].open) && (servers[req.body].password == null || servers[req.body].password == req.headers.password)){
			servers[req.body].authorized.push(req.cookies.userID);
			res.status(200).send('success');
		}
		else res.status(400).send('bad request');
		break;
		case 'createserver':
		if(servers[req.body] == undefined && req.body && req.body.match(/[^\w ]/) == null && decodeURIComponent(req.body).length <= 24){ 
			/*require('fs').readdir(__dirname + '/lobby', (err, files) => {
				files.forEach(file => {
					console.log(file);
				});
			});*/
			var app;
			if(process.platform.match(/win/) !== null) {
				if(require('child_process').spawnSync('dotnet build', {cwd: "lobby/", shell:true}).stderr.toString() == "") 
					app = cp.spawn('dotnet run', {cwd: "lobby/", shell:true, stdio:'pipe'});
				else app = cp.spawn('lobby.exe', {cwd: "lobby/bin/Debug/netcoreapp3.1/win-x64/", shell:true, stdio:'pipe'});
			}
			else app = cp.spawn('mono lobby.exe', {cwd: "lobby/", shell:true, stdio:'pipe'});
			var server = req.body;
			app.stderr.on('data', (chunk)=>{
				console.error('lobby error:\n' + chunk.toString());
				for(var ws in servers[server].backdoors)
					servers[server].backdoors[ws].send('lobby error:\n' + chunk.toString());
			});
			app.on('exit', () => {
				if(servers[server]){
					servers[server].down = true;
					for(var i in servers[server].WS) servers[server].WS[i].close();
				}
			});
			var msgqueue = '';
			app.stdout.on('data', (chunk)=>{
				//console.log('\u001b[95mSERVER:\u001b[0m'+encodeURIComponent(chunk.toString()));
				msgqueue+=chunk.toString();
				msgcheck();
			});
			function msgcheck(){
				while(msgqueue.includes(';')){
					msgeval(msgqueue.substring(0, msgqueue.indexOf(';')));
					msgqueue = msgqueue.substring(msgqueue.indexOf(';')+1);
				}
			}
			function msgeval(input){
				if(input.substring(0,3) == '!~~'){ 
					var arr2 = input.substring(3).split(':');
					switch(arr2[0]){
						case 'unlist':
						servers[server].open = false;
						break;
						case 'to':
						var id = IDs[arr2[1]];
						arr2.splice(0, 2);
						servers[server].WS[id].send(arr2.join(':')+';');
						var log = "\u001b[92mTO "+names[id]+":\u001b[0m "+colorMarkup(arr2.join(':'));
						console.log(log);
						for(var ws in servers[server].backdoors)
							servers[server].backdoors[ws].send('-'+convert.toHtml(log));
						break;
						case 'kick':
						servers[server].WS[IDs[arr2[1]]].close();
						delete servers[server].WS[userID];
						if(servers[server].connected.includes(userID))
							servers[server].connected.splice(servers[server].connected.indexOf(userID), 1);
						break;
					}
				}
				else if(input.substring(0,2) == '!~'){
					var log = "\u001b[92mSERVER:\u001b[0m "+colorMarkup(input.substring(2));
					console.log(log);
					for(var ws in servers[server].backdoors)
						servers[server].backdoors[ws].send('-'+convert.toHtml(log));
					for(var i in servers[server].WS)
						servers[server].WS[i].send(input.substring(2)+';');
				}
				else if(input.substring(0,1) == '!'){
					var matches = input.match(/^!(\w+) ([\s\S]+)/);
					for(var ws in servers[server].backdoors)
						if(ws == matches[1])
							servers[server].backdoors[ws].send(matches[2]);
				}
				else {
					var log = "\u001b[95mDEBUG:\u001b[0m "+input;
					console.log(log);
					for(var ws in servers[server].backdoors)
						servers[server].backdoors[ws].send('-'+convert.toHtml(log));
				}
			}
			servers[server] = {
				authorized:[req.cookies.userID],
				password:(req.headers.password !== '' ? req.headers.password : null),
				users: [],
				connected: [],
				name: server,
				process: app,
				WS:{},
				backdoors:{},
				open:true,
				down:false
			};
			res.status(200).send('success');
		}
		else res.status(409).send('failure');
		break;
		case 'setcolor':
		if(names[req.cookies.userID] !== undefined && decodeURIComponent(req.body).match(/^\d{1,3}$/) !== null){
			db.query("SELECT themehue FROM users WHERE username = $1", [names[req.cookies.userID]], (err, ret) =>{
				if(ret.rows.length == 1){
					db.query("UPDATE users SET themeHue = $1 WHERE username = $2", [req.body, names[req.cookies.userID]], (err) =>{
						if(err) res.status(500).send('an error has occured');
						else res.status(200).send('success');
					});
				}
				else res.status(500).send('an error has occured');
			});
		}
		break;
		case 'login':
		if(req.body && decodeURIComponent(req.body).length <= 24 && req.headers.username && req.headers.username.length <= 24 && req.body){
			req.headers.username = encodeURIComponent(req.headers.username);
			db.query("SELECT password FROM users WHERE username = $1", [req.headers.username], (err, ret) =>{
				if(ret.rows.length == 1){
					var hash = crypto.createHash('sha256');
					hash.update(decodeURIComponent(req.body));
					var pw = hash.digest('hex');
					if(ret.rows[0].password == pw){
						var id = IDgen();
						if(req.cookies.userID !== undefined) delete names[req.cookies.userID];
						IDs[req.headers.username] = id;
						names[id] = req.headers.username;
						res.append('userID', id);
						res.status(200).send('success');
					}
					else res.status(403).send('incorrect password');
				}
				else res.status(400).send('username not found');
			});
		}
		else res.status(403).send('Invalid login');
		break;
		case 'register':
		if(req.body && decodeURIComponent(req.body).length <= 24 && req.headers.username && req.headers.username.length > 0 && req.headers.username.length <= 24 && req.headers.username !== 'system' && req.headers.username.match(/^\d{1,2}$/) == null && encodeURIComponent(req.headers.username) == req.headers.username && req.headers.username.match(/[!~]/g) == null){
			req.headers.username = encodeURIComponent(req.headers.username);
			db.query("SELECT username FROM users WHERE username = $1", [req.headers.username], (err, ret) =>{
				if(ret.rows.length !== 0)res.status(400).send('that username has already been taken');
				else  if(decodeURIComponent(req.body).length < 6)res.status(400).send('your password must be 6 charecters or longer');
				else{
					var hash = crypto.createHash('sha256');
					hash.update(decodeURIComponent(req.body));
					var pw = hash.digest('hex');
					db.query("INSERT INTO users (username, password) VALUES ($1, $2)", [req.headers.username, pw], (err) =>{
						if(err) {console.error(err);res.status(500).send('there has been an error in creating your account, please try later');}
						else{
							var id = IDgen();
							IDs[req.headers.username] = id;
							names[id] = req.headers.username;
							res.append('userID', id);
							res.status(200).send('success');
						}
					});
				}
			});
		}
		else res.status(400).send("invalid username");
		break;
		case 'logout':
		if(req.cookies.userID){
			for(var i in connections){
				connections[i].close();
			}
			delete IDs[names[req.cookies.userID]];
			delete names[req.cookies.userID];
			res.status(200).send('success');
		}
		else res.status(400).send('failure');
		break;
		default:
		res.status(400).send('bad request');
	}
});

app.ws('/ws', (ws, req) =>{
	console.log('\u001b[95mwebsocket connected\u001b[0m');
	try{
		if(servers[req.ws.protocol] !== undefined && !servers[req.ws.protocol].connected.includes(req.cookies.userID) && servers[req.ws.protocol].authorized.includes(req.cookies.userID)){
			var server = req.ws.protocol;
			var userID = req.cookies.userID;
			var user = names[req.cookies.userID];
			var open = !!servers[server].open;
			servers[server].users.push(userID);
			servers[server].WS[userID] = ws;
			if(open)servers[server].connected.push(userID);
			if(connections[userID] == undefined) connections[userID] = [];
			connections[userID].push(ws);
			ws.on('message', function(msg) {
				if(msg == "dispose") return;
				var log = "\u001b[93m"+user+":\u001b[0m "+colorMarkup(msg);
				console.log(log);
				for(var ws in servers[server].backdoors)
					servers[server].backdoors[ws].send('-'+convert.toHtml(log));
				var arr = msg.split(':');
				for(var i in arr){
					arr[i] = encodeURIComponent(arr[i]);
				}
				msg = arr.join(':');
				servers[server].process.stdin.write(user + ':' + msg.toString() + ';');
			});
			ws.on('close', function() {
				connections[userID].splice(connections[userID].indexOf(ws), 1);
				servers[server].users.splice(servers[server].users.indexOf(userID), 1);
				delete servers[server].WS[userID];
				if(Object.keys(servers[server].WS).length == 0){
					if(process.platform.match(/win/) !== null) cp.spawn("taskkill", ["/pid", servers[server].process.pid, '/f', '/t']);
					else servers[server].process.kill();
					for(var i in servers[server].backdoors) servers[server].backdoors[i].disconnectBackdoor();
					delete servers[server];
				}
				else if ((open || servers[server].connected.includes(userID)) && !servers[server].down){
					servers[server].process.stdin.write(`system:userleft:${user};`);
					servers[server].connected.splice(servers[server].connected.indexOf(userID), 1);
				}
			});
			if(open) servers[server].process.stdin.write(`system:userjoined:${user};`);
			else{
				/*ws.send('logdata');
				servers[server].needhtml.push(ws);
				for(var i in servers[server].connected){
					servers[server].WS[servers[server].connected[i]].send('gethtml');
					break;
				}*/ //this is the code for spectators joining TODO: FIX THIS
			}
		}
		else if(servers[req.ws.protocol] == undefined)
			ws.close();
		else
			ws.close();
	} catch(err){console.error(err);}
});



var backdoor = {
	d3sync:"096df6ad7b2ef692ffcee69ce3e3d695f0f282112d1021d459b7e30a3cd9be03",
	cryoflame:"a963783e77f407e679ac96ace9a0fe647bf365f44d1968e57b8eb07b87e83113"
}
app.get('/backdoor', function(req, res){
	res.render('backdoor'); 
});
app.ws('/backdoor', (ws, req)=>{
	var authenticated = false;
	var username = "";
	ws.send("Enter Username");
	ws.on('message', (msg)=>{
		if(msg == "dispose" || authenticated) return;
		if(username == ""){
			if(backdoor[msg.toLowerCase()] == undefined){
				ws.send("Invalid username");
			}
			else{
				username = msg;
				ws.send("Enter Password");
			}
		}
		else {
			var hash = crypto.createHash('sha256');
			hash.update(msg);
			var pass = hash.digest('hex');

			if(backdoor[username.toLowerCase()] != pass){
				ws.send("Invalid password");
			}
			else{
				ws.send("Backdoor unlocked. Executive commands authorized");
				initialize();
			}
		}
		
	});

	var lobby = "";
	function initialize(){
		authenticated = true;
		console.log('\u001b[95muser '+username+' connected to backdoor\u001b[0m');
		lobby = "";
		ws.disconnectBackdoor = ()=>{
			lobby = "";
			console.log('\u001b[95muser '+username+' disconnected from lobby '+lobby+' backdoor\u001b[0m');
			ws.send("You have been disconnected from the lobby because the lobby has shut down");
			ws.send("Main Menu");
		}
		ws.send("Main Menu");
		ws.on('message', function(msg) {
			if(msg == "dispose") return;
			console.log("\u001b[95m"+username+":\u001b[0m "+colorMarkup(msg));
			if(lobby == ""){
				var cmd = msg.match(/^(\w+)/);
				if(!cmd) {
					ws.send("Invalid commmand");
					return;
				}
				var args = msg.substring(cmd[0].length+1);
				switch(cmd[1]){
					case "connect": 
					if(servers[args] != undefined){
						lobby = args;
						servers[lobby].backdoors[username] = ws;
						ws.send("backdoor connected to lobby "+lobby);
						console.log('\u001b[95muser '+username+' connected to lobby backdoor '+lobby+'\u001b[0m');
					}
					else{
						var lobbies = 'Lobbies:';
						for(let s in servers) lobbies +=s+',';
						ws.send(lobbies.substring(0, lobbies.length-1));
					}
					break;
					case "help":
					ws.send('Commands:\n'+
					'\tconnect:\tconnect to a lobby in order to send it commands');
					break;
					default:
					ws.send("Command not found");
					return;
				}
			}
			else{
				var cmd = msg.match(/^(\w+)/);
				if(!cmd) {
					ws.send("Invalid commmand");
					return;
				}
				var args = msg.substring(cmd[0].length+1);
				switch(cmd[1]){
					case "exec": 
					try{
						arr = [];
						while(true){
							if(args.substring(0, 1) == '"'){
								var index = args.substring(1).indexOf('"')+1;
								if(index == 0) throw 'Invalid command';
								arr.push(args.substring(1, index));
								args = args.substring(index+1);
							}
							else {
								var index = args.indexOf(' ');
								if(index == -1){
									arr.push(args);
									break;
								}
								else{
									arr.push(args.substring(0, index));
									args = args.substring(index+1);
								}
							}
						}
						servers[lobby].process.stdin.write(`system:backdoor:${username}:${arr.join(':')};`);
					}
					catch(err){
						ws.send("Invalid commmand");
					}
					break;
					case "disconnect":
					delete servers[lobby].backdoors[servers[lobby].backdoors.indexOf(ws)];
					lobby = "";
					console.log('\u001b[95muser '+username+' disconnected from lobby '+lobby+' backdoor\u001b[0m');
					ws.send("Main Menu");
					break;
					case "kill":
					if(process.platform.match(/win/) !== null) cp.spawn("taskkill", ["/pid", servers[server].process.pid, '/f', '/t']);
					else servers[server].process.kill();
					break;
					case "help":
					ws.send('Commands:\n'+
					'\tdisconnect:\tdisconnect from the current lobby and return to the main menu\n'+
					'\texec:\texecute a lobby command, use \"exec help\" to get lobby commands'+
					'\tkill:\tkill the lobby you are currently connected to');
					break;
					default:
					ws.send("Command not found");
					return;
				}
			}
		});
		ws.on('close', function() {
			if(lobby != ""){
				delete servers[lobby].backdoors[servers[lobby].backdoors.indexOf(ws)];
				console.log('\u001b[95muser '+username+' disconnected from lobby '+lobby+' backdoor\u001b[0m');
			}
			console.log('\u001b[95muser '+username+' disconnected from backdoor\u001b[0m');
		});
	}
	
});



app.listen(port, ()=>{
	console.log('\u001b[95mThe website is now being hosted\u001b[0m');
}); 


//*database
process.env.PGUSER = process.env.PGUSER || 'user_name';
process.env.PGPASSWORD = process.env.PGPASSWORD || 'password';
var db = new Client({
	connectionString: connection,
	ssl: process.platform.match(/win/) == null
});
db.connect();


db.query('CREATE TABLE users()', (err) =>{if(err && err.code !== '42P07') console.error(err);});
db.query('ALTER TABLE users ADD username VARCHAR(72)', (err) =>{if(err && err.code !== '42701') console.error(err);});
	//username is stored url-encoded
db.query('ALTER TABLE users ADD password CHAR(64)', (err) =>{if(err && err.code !== '42701') console.error(err);});
	//password is stored as a hashed urldecoded hex-value
db.query('ALTER TABLE users ADD themeHue SMALLINT', (err) =>{if(err && err.code !== '42701') console.error(err);});

function IDgen(){
	var chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890"
	var id = '';
	for (let i = 0; i < 10; i++) {
		id += chars.charAt(Math.floor(Math.random() * chars.length))
	}
	if(names[id] == undefined) return id;
	else return IDgen();
}/**/