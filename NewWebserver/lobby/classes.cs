using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;

namespace classes{
    public abstract class Entity{
        public override string ToString() {return name + " - " + id;}
        protected bool canTakeDamage = true;
        public static Random rand = new Random();
        protected string name;
        public string Name{
            get{
                return name;
            }
        }
        protected string id;
        public string Id{
            get{
                return id;
            }
        }
        protected int health;
        public int Health{
            get{
                return health;
            }
            set{
                if(canTakeDamage == false && value <= health) return;
                int prev = health;
                health = value;
                if(health > MaxHealth) health = MaxHealth;
                else if(health < 0) health = 0;
                loopThrough(b => b.GetActivation<AfterHealthChanged>().D(prev));
                Lobby.updateEntityStats(this);
                if(health == 0 && alive){
                    buffs = new Dictionary<string, Buff>();
                    resetChangeBuff();
                    alive = false;
                    die();
                }
            }
        }
        protected bool alive = true;
        public bool Alive{
            get{
                return alive;
            }
        }
        protected int maxHealth;
        public int MaxHealth{
            get{
                return maxHealth;
            }
            set{
                maxHealth = value;
                if(health > maxHealth) health = maxHealth;
                Lobby.updateEntityStats(this);
            }
        }
        public Dictionary<string, Buff> buffs = new Dictionary<string, Buff>();
        List<Buff> removeQueue = new List<Buff>();
        List<Buff> addQueue = new List<Buff>();
        List<Buff> removeHookQueue = new List<Buff>();
        List<Buff> addHookQueue = new List<Buff>();
        bool changingBuff = false;
        int buffLayer = 0;
        public void AllowChangeBuff(bool allow){
            buffLayer += (allow?-1:1);
            if(buffLayer < 0) buffLayer = 0;
            if(buffLayer == 0 && !changingBuff){
                if(!alive){
                    resetChangeBuff();
                    return;
                }
                changingBuff = true;
                for(int i = 0; i < addQueue.Count; i++) addBuff(addQueue[i]);
                for(int i = 0; i < removeQueue.Count; i++) removeBuff(removeQueue[i]);
                for(int i = 0; i < addHookQueue.Count; i++) addHook(addHookQueue[i]);
                for(int i = 0; i < removeHookQueue.Count; i++) removeHook(removeHookQueue[i]);
                addQueue.Clear();
                removeQueue.Clear();
                addHookQueue.Clear();
                removeHookQueue.Clear();
                changingBuff = false;
            }
        }

        public void resetChangeBuff(){
            buffLayer = 0;
            addQueue.Clear();
            removeQueue.Clear();
            addHookQueue.Clear();
            removeHookQueue.Clear();
        }

        public void loopThrough(Action<Buff> loop){
            AllowChangeBuff(false);
            List<Buff> list = new List<Buff>(buffs.Values);
            list.Sort((b1, b2) => b1.Duration - b2.Duration);
            foreach(Buff b in list) loop(b);
            AllowChangeBuff(true);
        }

        public Entity(int health, string id){
            this.health = health;
            this.maxHealth = health;
            this.id = id;
        }

        public virtual void die(){}
        public virtual void revive(int hp){}

        public List<Buff> hooked = new List<Buff>();
        public void addHook(Buff buff){
            if(buffLayer == 0) hooked.Add(buff);
            else addHookQueue.Add(buff);
        }
        public void removeHook(Buff buff){
            if(buffLayer == 0) hooked.Remove(buff);
            else removeHookQueue.Add(buff);
        }

        string genID(){
            string id;
            char[] arr = new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'};
            do{
                id = "";
                for(int i = 0; i < 3; i++) id += arr[rand.Next(arr.Length)];
            }while (buffs.ContainsKey(id));
            return id;
        }

        public Buff addBuff(Buff buff){
            if(!alive) return buff;
            if(buffLayer == 0){
                if(buffs.ContainsValue(buff)) return buff;

                string id = genID();
                buff.setHost(this);
                buff.setID(id);

                bool rangeUpdate = false;
                Enemy e = Lobby.getEnemyFromID(buff.Self.Id);
                if(e != null && e.PlannedAction != null && e.PlannedAction.HitCount > 0){
                    if(buff.locals.ContainsKey("Strength") || buff.locals.ContainsKey("Intellect"))
                        rangeUpdate = true;
                }

                buffs.Add(id, buff);
                if(buff.Caster != null) buff.Caster.addHook(buff);
                Lobby.buffAdded(buff);
                if(rangeUpdate) Lobby.attackSeverityChanged(e);
                Lobby.loopThroughEntities(entity=>entity.buffApplied(buff));
            }
            else if(!addQueue.Contains(buff)) {
                addQueue.Add(buff);
            }
            return buff;
        }

        public void removeBuff(Buff buff){

            buff.GetActivation<OnExpire>().D();
            buff.nullify();

            if(buffLayer == 0){
                buffs.Remove(buff.Id);
                if(buff.Caster != null) buff.Caster.removeHook(buff);
                Lobby.buffExpire(buff);
            }
            else removeQueue.Add(buff);
        }

        public virtual void resetEffects(){
            resetChangeBuff();
            hooked = new List<Buff>();
            buffs = new Dictionary<string, Buff>();
        }

        public int getTotalValue(string key){
            int val = 0;
            foreach(Buff b in buffs.Values) if(b.locals.ContainsKey(key)) val += b.locals[key];
            return val;
        }

        public Buff addTrapBuff(string name, int duration, Entity castor, Action<int?, Entity, DamageType, AttackType> callback){ 
            if(name == "") return null;
            Buff b = new Buff(name, duration, typeof(AfterCastorTurn), castor, BuffClass.Trap);
            b.AddActivation(new AfterTryDefend((damage, target, Dtype, Atype)=>{
                if(Atype != AttackType.Daggers && Atype != AttackType.Smite && Atype != AttackType.Burst) 
                    callback(damage, target, Dtype, Atype);
            }));
            addBuff(b);
            return b;
        }

        public Buff addHexBuff(string name, int threshhold, int duration, Entity castor, Action callback){
            if(name == "") return null;
            Buff b = new Buff(name, duration, typeof(AfterCastorTurn), castor, BuffClass.Hex);
            b.locals.Add("x", threshhold);
            b.AddActivation(new AfterDamage((damage)=>{
                if(!b.locals.canChange) return;
                int rem = damage % threshhold;
                for(int i = 0; i < (damage - rem) / threshhold; i++)
                    callback();
                int next = b.locals["x"] - rem;
                if(next <= 0){
                    next += threshhold;
                    callback();
                }
                b.locals["x"] = next;
            }));
            addBuff(b);
            return b;
        }

        public static bool UniqueBuffExists(string key){
            try{
                new Enemy().addUniqueLocal(key, 1, null);
                return true;
            }
            catch{
                return false;
            }
        }

        public Buff addUniqueLocal(string key, int duration, Entity caster){
            bool cont = true;
            foreach(Buff b in buffs.Values){
                if(b.locals.ContainsKey(key)){
                    cont = false;
                    b.Duration += duration;
                    break;
                }
            }
            foreach(Buff b in addQueue){
                if(b.locals.ContainsKey(key)){
                    cont = false;
                    b.Duration += duration;
                    break;
                }
            }
            Buff buff = null;
            if(cont){
                Type dtype;
                switch(key){
                    case "Daggers": case "Burst": dtype = null; break;
                    case "Freeze": case "Blind": case "Stun": case "Smite": dtype = typeof(AfterTurn); break;
                    case "Fire": case "Hellfire": case "Poison": dtype = typeof(EndRound); break;
                    default: throw new Exception("Duration type for unique buff "+key+" not found");
                }
                if(name == "") return null;
                buff = new Buff(key, duration, dtype, null);
                buff.locals.Add(key, 1);
                addEffect(buff, key);
                addBuff(buff);
            }
            Lobby.loopThroughEntities(entity=>entity.uniqueLocalApplied(key, duration, caster, this));
            return buff;
        }

        public static bool StatBuffExists(string key){
            try{
                new Enemy().addStatBuff(key, 1, 1, null);
                return true;
            }
            catch{
                return false;
            }
        }

        public Buff addStatBuff(string key, int power, int duration, Entity caster){
            return addStatBuff("", key, power, duration, caster, false);
        }

        public Buff addStatBuff(string key, int power, int duration, Entity caster, bool decaying){
            return addStatBuff("", key, power, duration, caster, decaying);
        }

        public Buff addStatBuff(string name, string key, int power, int duration, Entity caster){
            return addStatBuff(name, key, power, duration, caster, false);
        }

        public Buff addStatBuff(string name, string key, int power, int duration, Entity caster, bool decaying){
            if(name == "") name = key;
            Type dtype = null;
            switch(key){
                case "Strength": case "Intellect": dtype = typeof(AfterTurn); break;
                case "Armor": case "Resolve": dtype = typeof(AfterCastorTurn); break;
                case "Shield": case "Dodge": dtype = typeof(StartRound); break;
                case "Regen": dtype = typeof(EndRound); break;
                default: throw new Exception("Duration type for stat buff "+key+" not found");
            }
            if(name == "") return null;
            Buff b = new Buff(name, duration, dtype, caster);
            b.locals.Add(key, power);
            addEffect(b, key);
            if(decaying) b.AddDecay(key);
            addBuff(b);
            return b;
        }

        public static Buff createStatBuff(string name, string key, int power, int duration, Entity caster){
            if(name == "") name = key;
            Type dtype = null;
            switch(key){
                case "Strength": case "Intellect": dtype = typeof(AfterTurn); break;
                case "Armor": case "Resolve": dtype = typeof(AfterCastorTurn); break;
                case "Shield": case "Dodge": dtype = typeof(StartRound); break;
                case "Regen": dtype = typeof(EndRound); break;
                default: throw new Exception("Duration type for stat buff "+key+" not found");
            }
            if(name == "") return null;
            Buff b = new Buff(name, duration, dtype, caster);
            b.locals.Add(key, power);
            return b;
        }

        public static void addEffect(Buff b, string effect){
            switch(effect){
                case "Strength": 
                b.AddActivation(new OnAttack((damage, target, Dtype, Atype)=>{
                    if(Dtype == DamageType.Physical) return damage + b.locals["Strength"] / (Atype == AttackType.Burst || Atype == AttackType.Daggers ? 2 : 1);
                    else return damage;
                }));
                break;
                case "Intellect":
                b.AddActivation(new OnAttack((damage, target, Dtype, Atype)=>{
                    if(Dtype == DamageType.Magic) return damage + b.locals["Intellect"] / (Atype == AttackType.Burst || Atype == AttackType.Daggers ? 2 : 1);
                    else return damage;
                }));
                break;
                case "Armor": 
                b.AddActivation(new OnDefend((damage, attacker, Dtype, Atype)=>{
                    if(Dtype == DamageType.Physical) return damage - b.locals["Armor"] / (Atype == AttackType.Burst || Atype == AttackType.Daggers ? 2 : 1);
                    else return damage;
                }));
                break;
                case "Resolve":
                b.AddActivation(new OnDefend((damage, attacker, Dtype, Atype)=>{
                    if(Dtype == DamageType.Magic) return damage - b.locals["Resolve"] / (Atype == AttackType.Burst || Atype == AttackType.Daggers ? 2 : 1);
                    else return damage;
                }));
                break;
                case "Shield":
                b.AddActivation(new OnDamage((damage)=>{
                    int sub = damage <= b.locals["Shield"] ? damage : b.locals["Shield"];
                    damage -= sub;
                    b.locals["Shield"] -= sub;
                    if(b.locals["Shield"] == 0) b.Self.removeBuff(b);
                    return damage;
                }));
                break;
                case "Dodge":
                b.AddActivation(new OnTryDefend((hit, damage, attacker, Dtype, Atype)=>{
                    if(hit){
                        hit = false;
                        if(Atype != AttackType.Burst && Atype != AttackType.Daggers) b.locals["Dodge"]--;
                        if(b.locals["Dodge"] == 0) b.Self.removeBuff(b);
                    }
                    return hit;
                }));
                break;
                case "Regen":
                b.AddActivation(new EndRound(()=>{
                    b.Self.Heal(b.locals["Regen"], null);
                }));
                break;



                case "Daggers":
                b.AddActivation(new AfterAttack((damage, target, Dtype, Atype)=>{
                    if(Atype == AttackType.Base && Dtype == DamageType.Physical && target.Alive) {
                        b.Self.Attack(3, target, DamageType.Physical, AttackType.Daggers);
                        b.Duration--;
                    }
                }));
                break;
                case "Burst":
                b.AddActivation(new AfterAoE((damage, hits, Dtype, burst)=>{
                    if(!burst && Dtype== DamageType.Magic) {
                        b.Self.AttackAoE(b.Duration, DamageType.Magic, true, null);
                        b.Self.removeBuff(b);
                    }
                }));
                break;
                case "Smite":
                b.AddActivation(new AfterAttack((damage, target, Dtype, Atype)=>{
                    if(Atype == AttackType.Base) {
                        b.Self.Attack(5, target, DamageType.Magic, AttackType.Smite);
                    }
                }));
                break;
                case "Freeze":
                break;
                case "Stun":
                b.AddActivation(new AfterDefend((damage, attacker, Dtype, Atype)=>{
                    b.Self.removeBuff(b);
                }));
                break;
                case "Blind":
                b.AddActivation(new OnTryAttack((hit, damage, target, Dtype, Atype)=>{
                    return false;
                }));
                break;
                case "Fire":
                b.AddActivation(new StartRound(()=>{
                    b.Self.takeDamage(3);
                }));
                break;
                case "Hellfire":
                b.AddActivation(new StartRound(()=>{
                    b.Self.takeDamage(5);
                }));
                break;
                case "Poison":
                b.AddActivation(new StartRound(()=>{
                    b.Self.takeDamage(b.Duration);
                    b.Duration -= b.Duration / 10;
                }));
                break;
                    
                default: throw new Exception("Stat or unique buff "+b.Name+" not recognized");
            }
        }

        public int getUniqueLocal(string key){
            foreach(Buff b in buffs.Values) if(b.locals.ContainsKey(key)) return b.Duration;
            return 0;
        }

        public Buff getUniqueBuff(string key){
            foreach(Buff b in buffs.Values) if(b.locals.ContainsKey(key)) return b;
            return null;
        }

        public int AttackAoE(int damage, DamageType Dtype){
            return AttackAoE(damage, Dtype, null);
        }

        public int AttackAoE(int damage, DamageType Dtype, Action<int?, Entity> callback){
            return AttackAoE(damage, Dtype, false, callback);
        }

        public int AttackAoE(int damage, DamageType Dtype, bool burst, Action<int?, Entity> callback){
            Entity[] targets;
            if(Lobby.getPlayerFromID(id) != null) targets = Lobby.enemies.ToArray();
            else targets = new List<Player>(Lobby.players.Values).ToArray();

            int total = 0;
            int hits = 0;
            foreach(Entity e in targets){
                int? atk = Attack(damage, e, Dtype, burst ? AttackType.Burst : AttackType.AoE);
                if(callback != null) callback(atk, e);
                if(atk != null) {
                    total += (int)atk;
                    hits++;
                }
            }

            loopThrough(b => b.GetActivation<AfterAoE>().D(total, hits, Dtype, burst));

            return total;
        }

        public int AttackI(int damage, Entity target, DamageType Dtype){
            return AttackI(damage, target, Dtype, AttackType.Base);
        }

        public int AttackI(int damage, Entity target, DamageType Dtype, AttackType Atype){
            int? dmg = Attack(damage, target, Dtype, Atype);
            return dmg == null ? 0 : (int)dmg;
        }

        public int? Attack(int damage, Entity target, DamageType Dtype){
            return Attack(damage, target, Dtype, AttackType.Base);
        }

        public int? Attack(int damage, Entity target, DamageType Dtype, AttackType Atype){
            if (alive){
                int? res; 
                bool hitChance = true;
                loopThrough(b => hitChance = b.GetActivation<OnTryAttack>().D(hitChance, damage, target, Dtype, Atype));
                target.loopThrough(b => hitChance = b.GetActivation<OnTryDefend>().D(hitChance, damage, this, Dtype, Atype));
                if(hitChance){
                    if(damage != 0){
                        loopThrough(b => {int d = b.GetActivation<OnAttack>().D(damage, target, Dtype, Atype); damage = d < 0 ? 0 : d;});
                        damage = target.Defend(this, damage, Dtype, Atype);
                        loopThrough(b => b.GetActivation<AfterAttack>().D(damage, target, Dtype, Atype));
                    }
                    res = damage;
                }
                else res = null;
                target.loopThrough(b => b.GetActivation<AfterTryDefend>().D(res, this, Dtype, Atype));
                loopThrough(b => b.GetActivation<AfterTryAttack>().D(res, target, Dtype, Atype));
                return res;
            } else return null;
        }
        public int Defend(Entity attacker, int damage, DamageType Dtype, AttackType Atype){
            if(alive){
                loopThrough(b => {int d = b.GetActivation<OnDefend>().D(damage, attacker, Dtype, Atype);  damage = d < 0 ? 0 : d;});
                damage = takeDamage(damage);
                loopThrough(b => b.GetActivation<AfterDefend>().D(damage, attacker, Dtype, Atype));
                return damage;
            }
            return 0;
        }

        public int takeDamage(int damage){
            if(alive){
                loopThrough(b => {int d = b.GetActivation<OnDamage>().D(damage);  damage = d < 0 ? 0 : d;});
                int temp = Health;
                Health -= damage;
                damage = temp - Health;
                loopThrough(b => b.GetActivation<AfterDamage>().D(damage));
                return damage;
            }
            return 0;
        }

        public int Heal(int heal, Entity caster){
            if(caster != null) caster.loopThrough(b => heal = b.GetActivation<OnApplyHeal>().D(heal, this));
            loopThrough(b => heal = b.GetActivation<OnHeal>().D(heal, caster));
            int temp = Health;
            Health += heal;
            heal = Health - temp;
            loopThrough(b => b.GetActivation<AfterHeal>().D(heal, caster));
            if(caster != null) caster.loopThrough(b => b.GetActivation<AfterApplyHeal>().D(heal, this));

            if(!Alive && Health >= MaxHealth / 2)
                revive(Health);

            return heal;
        }

        public void beforeTurn(){
            loopThrough(b => b.GetActivation<BeforeTurn>().D());
        }

        public void afterTurn(){
            loopThrough(b => b.GetActivation<AfterTurn>().D());
            AllowChangeBuff(false);
            for(int i = 0; i < hooked.Count; i++) hooked[i].GetActivation<AfterCastorTurn>().D();
            AllowChangeBuff(true);
        }

        public void buffApplied(Buff buff){
            loopThrough(b => b.GetActivation<AfterBuffApplied>().D(buff));
        }

        public void uniqueLocalApplied(string key, int value, Entity castor, Entity target){
            loopThrough(b => b.GetActivation<AfterUniqueLocalAdded>().D(key, value, castor, target));
        }

        public void turnTaken(){
            loopThrough(b => b.GetActivation<TurnTaken>().D());
        }

        public void startRound(){
            loopThrough(b => b.GetActivation<StartRound>().D());
        }

        public void endRound(){
            loopThrough(b => b.GetActivation<EndRound>().D());
        }

    }
    public abstract class Player : Entity{
        public abstract Type Class {get;}
        private int resource = 0;
        protected int maxResource = 100;
        protected int minResource = 0;
        
        public int Resource{
            get{
                return resource;
            }
            set{
                loopThrough(b=>value = b.GetActivation<OnResourceChange>().D(value));
                if(value >= maxResource) resource = maxResource;
                else if(value <= minResource) resource = minResource;
                else resource = value;
                Lobby.updateEntityStats(this);
                if(resource == 0) endClassAbility();
            }
        }
        private bool classAbilityActive = false;
        public bool ClassAbilityActive {
            get{
                return classAbilityActive;
            }
            protected set{
                Lobby.cmd("playerClassAbility:"+name+":"+(value?"1":"0"));
                classAbilityActive = value;
            }
        }
        public abstract void initalizeSkillTree();
        public abstract void activateClassAbility();
        public abstract void endClassAbility();

        protected SkillTree skillTree;
        public SkillTree ClassTree{
            get{
                return skillTree;
            }
        }

        List<Card> starterPool = new List<Card>();
        List<Card> beginnerPool = new List<Card>();
        List<Card> intermediatePool = new List<Card>();
        List<Card> advancedPool = new List<Card>();
        /*public List<string> StarterPool {
            get{
                return starterPool;
            }
        }
        public List<string> BeginnerPool {
            get{
                return beginnerPool;
            }
        }
        public List<string> IntermediatePool {
            get{
                return intermediatePool;
            }
        }
        public List<string> AdvancedPool {
            get{
                return advancedPool;
            }
        }*/
        List<Card> currentPool = new List<Card>();
        public Card[] CurrentPool {
            get{
                return currentPool.ToArray();
            }
        }
        Dictionary<string, Card> totalDeck = new Dictionary<string, Card>();
        List<Card> deck = new List<Card>();
        List<Card> hand = new List<Card>();
        List<Card> discard = new List<Card>();
        public Card[] Hand {
            get{
                return hand.ToArray();
            }
        }
        public Card[] TotalDeck {
            get{
                return new List<Card>(totalDeck.Values).ToArray();
            }
        }
        /*public List<string> Deck {
            get{
                return deck;
            }
        }
        public List<string> Discard {
            get{
                return discard;
            }
        }*/
        public Player(string name, int health):base(health, name){
            this.name = name;
            initalizeSkillTree();
        }
        public override void resetEffects(){
            endClassAbility();
            resetChangeBuff();
            hooked = new List<Buff>();
            buffs = new Dictionary<string, Buff>();
        }
        private List<Action> encounterEffects = new List<Action>();
        public void AddEncounterEffect(Action a){
            encounterEffects.Add(a);
        }
        private List<Action> encounterEndEffects = new List<Action>();
        public void AddEndEffect(Action a){
            encounterEndEffects.Add(a);
        }
        public void encounterEnd(){
            foreach(Card c in totalDeck.Values)
                if(c.OnEncounterEnd != null) c.OnEncounterEnd(this, c);
            foreach(Action a in encounterEndEffects)
                a();
        }
        public void encounterStart(){
            foreach(Card c in totalDeck.Values)
                if(c.OnEncounterStart != null) c.OnEncounterStart(this, c);
            foreach(Action a in encounterEffects)
                a();
        }
        
        public override void die(){
            endClassAbility();
            Resource = 0;
            Lobby.playerAliveChanged(this);
            Lobby.PlayerCheckDeath();
        }
        public override void revive(int hp){ 
            if(!alive){
                Health = hp;
                alive = true;
                Lobby.playerAliveChanged(this);
            }
        }
        public void Taunt(Enemy e){
            if(e.PlannedAction.Targets <= 0){ //note: game freezes if no intention has more than 0 targets
                do e.PlannedAction = e.Behaviors[rand.Next(e.Behaviors.Length)]; while(e.PlannedAction.Targets <= 0);
                e.PlannedAction.targets = new string[]{this.id};
            }
            else{
                List<string> targets = new List<string>();
                targets.Add(id);
                for(int i = 0; i < e.PlannedAction.targets.Length; i++)
                    if(e.PlannedAction.targets[i] == id)
                        targets.Add(id);
                e.PlannedAction.targets = targets.ToArray();
            }
            Lobby.enemyIntentionsChanged(e);
        }

        public void AddCardToHand(Card card){
            hand.Add(card);
            Lobby.CardAdded(card);
            totalDeck.Add(card.Id, card);
            Check3x(GetCardPool(card), card);
        }

        public Card[] DrawCards(int cards){
            List<Card> lst = new List<Card>();
            for(int a = 0; a < cards; a++){
                int i = rand.Next(0, deck.Count);
                hand.Add(deck[i]);
                lst.Add(deck[i]);
                Lobby.CardAdded(deck[i]);
                deck.RemoveAt(i);
                if(deck.Count == 0){
                    for(int b = 0; b < discard.Count; b++)
                        deck.Add(discard[b]);
                    Lobby.Shuffle(ref deck);
                    discard.Clear();
                }
            }
            return lst.ToArray();
        }
        public void GenerateCardPool(int encounterNum){
            picked = false;
            int cards = (encounterNum > 10 ? (encounterNum > 20 ? 5 : 4) : 3);
            if(encounterNum % 5 == 0) cards++;
            for(int i = 0; i < cards; i++){
                List<Card> pool;
                do pool = RandomCardPoolRarity(); while(pool.FindAll(c=>currentPool.Contains(c)).Count == pool.Count);
                Card card;
                do card = pool[rand.Next(pool.Count)]; while(currentPool.Contains(card));
                currentPool.Add(card);
            }
        }
        bool picked = false;
        public Card PickCardInPool(string card){
            Card c = null;
            if(currentPool.Find(ca=>card == ca.Name) != null) 
                c = AddCard(card);
            if(Lobby.encounterNumber % 5 == 0 && !picked) picked = true;
            else currentPool.Clear();
            return c;
        }
        List<Card> nullPool = null;
        private List<Card> GetCardPool(Card card){
            if(advancedPool.Contains(card)) return advancedPool;
            else if(intermediatePool.Contains(card)) return intermediatePool;
            else if(beginnerPool.Contains(card)) return beginnerPool;
            else if(starterPool.Contains(card)) return starterPool;
            else return nullPool;
        }
        private List<Card> RandomCardPoolRarity(){
            int max;
            if(beginnerPool.Count == 0) max = 1;
            else if(intermediatePool.Count == 0) max = 3;
            else if(advancedPool.Count == 0) max = 7;
            else max = 15;
            int randNum = rand.Next(max);
            switch(randNum){
                case 0: return starterPool;
                case 1: case 2: return beginnerPool;
                case 3: case 4: case 5: case 6: return intermediatePool;
                default: return advancedPool;
            }
        }
        public void ResetDeck(){
            DiscardCards();
            for(int b = 0; b < discard.Count; b++)
                deck.Add(discard[b]);
            Lobby.Shuffle(ref deck);
            discard.Clear();
        }
        public void DiscardCard(Card card){
            hand.Remove(card);
            discard.Add(card);
        }
        public void DiscardCards(){
            for(int i = 0; i < hand.Count; i++)
                discard.Add(hand[i]);
            hand.Clear();
        }
        public void RemoveCard(string cardId){
            Card c = totalDeck[cardId];
            deck.Remove(c);
            totalDeck.Remove(cardId);
            Lobby.cmd("removeCard:"+Name+":"+cardId);
        }
        public void AddCards(string[] cards){
            foreach(string c in cards) AddCard(c);
        }
        public Card AddCard(string card){
            Card c = Card.GetCard(card);
            c.setSelf(this, genCardID());
            deck.Add(c);
            totalDeck.Add(c.Id, c);
            Check3x(GetCardPool(c), c);
            if(!Lobby.Open) Lobby.cmd("addCardToDeck:"+this.Name+":"+c.Id+":"+c.Name+":"+Lobby.dicToStr(c.locals.Dict));
            return c;
        }
        string genCardID(){
            string id;
            char[] arr = new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'};
            do{
                id = "";
                for(int i = 0; i < 3; i++) id += arr[rand.Next(arr.Length)];
            }while (totalDeck.ContainsKey(id));
            return id;
        }
        public void AddToStarterPool(string[] cards){
            foreach(Card c in Card.GetCards(cards)) starterPool.Add(c);
            Check3x(starterPool);
        }
        public void AddToBeginnerPool(string[] cards){
            foreach(Card c in Card.GetCards(cards)) beginnerPool.Add(c);
            Check3x(beginnerPool);
        }
        public void AddToIntermediatePool(string[] cards){
            foreach(Card c in Card.GetCards(cards)) intermediatePool.Add(c);
            Check3x(intermediatePool);
        }
        public void AddToAdvancedPool(string[] cards){
            foreach(Card c in Card.GetCards(cards)) advancedPool.Add(c);
            Check3x(advancedPool);
        }
        public void Check3x(List<Card> input){
            for(int i = input.Count - 1; i >= 0; i--)
                Check3x(input, input[i]);
        }
        public void Check3x(List<Card> input, Card card){
            if(input == null) return;
            int count = 0;
            foreach(Card c in totalDeck.Values)
                if(c.Name == card.Name)
                    count++;
            if(count >= 3) input.RemoveAt(input.LastIndexOf(card));
        }
    }
    public partial class Enemy : Entity{
        public Dictionary<string, int> Locals = new Dictionary<string, int>();
        public void setID(string id){
            if(this.id == "") this.id = id;
        }

        Func<Enemy, Intention> targeting;
        Intention[] behaviors;
        public Intention[] Behaviors{
            get{
                return behaviors;
            }
        }
        Intention plannedAction = null;
        public Intention PlannedAction{
            get{
                return plannedAction;
            }
            set{
                plannedAction = value;
                if(value == null) return;
                if(plannedAction.Targets > 0){
                    List<Player> players = new List<Player>(Lobby.players.Values);
                    for(int i = players.Count - 1; i >= 0; i--)
                        if(!players[i].Alive) players.RemoveAt(i);
                    if(players.Count <= plannedAction.Targets){
                        plannedAction.targets = players.Select(p => p.Id).ToArray();
                    }
                    else{
                        List<string> tars = new List<string>();
                        for(int i = 0; i < plannedAction.Targets; i++) {
                            Player tar = null;
                            do {
                                tar = players[rand.Next(players.Count)]; 
                            } while (tars.Contains(tar.Id));
                            tars.Add(tar.Id);
                        }
                        plannedAction.targets = tars.ToArray();
                    }
                }
                Lobby.enemyIntentionsChanged(this);
            }
        }
        Action<Enemy> onSpawn;
        public Action<Enemy> OnSpawn{
            get{
                return onSpawn;
            }
        }
        private Func<Enemy, bool> recalc;

        public Enemy():base(10, ""){
            this.targeting = null;
            this.onSpawn = null;
            this.behaviors = new Intention[0];
            this.name = "";
        }

        public Enemy(string name, int health, Func<Enemy, Intention> targeting, Action<Enemy> spawnAction, Func<Enemy, bool> recalc, Intention[] behaviors):base(HealthAdjust(health), ""){
            this.targeting = targeting;
            this.onSpawn = spawnAction;
            this.behaviors = behaviors;
            this.name = name;
            this.recalc = recalc;
        }

        public static int HealthAdjust(int hp){
            int pn = Lobby.players.Count;
            hp = (int)((.25f * (pn - 2) + 1f) * ((float)hp));
            hp = (int)(((float)hp) * (rand.NextDouble() * .1f + .95f));
            return hp;
        }

        public void potentialRecalc(){
            if(recalc(this))
                CalcTarget();
        }

        public override void die(){
            Lobby.enemies.Remove(this);
            Lobby.enemyDied(this);
            Lobby.EnemyCheckDeath();
            foreach(Enemy e in Lobby.enemies)
                e.potentialRecalc();
        }

        public void CalcTarget(){
            PlannedAction = targeting(this);
        }

        public void TakeTurn(){
            int num = (PlannedAction.Targets < 1 ? 1 : PlannedAction.Targets);
            if(PlannedAction.Targets > 0 && PlannedAction.targets.Length == 0) num = 0;
            for(int i = 0; i < num; i++)
                PlannedAction.Behavior(this, PlannedAction.Targets > 0?Lobby.getPlayerFromID(PlannedAction.targets[i]):null);
            Lobby.enemyTurnTaken(this);
            PlannedAction = null;
        }
        public int getDamage(){
            int dmg = 0; 
            if(PlannedAction.DamageType == DamageType.Physical) dmg = PlannedAction.HitCount * (PlannedAction.Damage + getTotalValue("Strength"));
            else if (PlannedAction.DamageType == DamageType.Magic) dmg = PlannedAction.HitCount * (PlannedAction.Damage + getTotalValue("Intellect"));
            if(dmg < 0) dmg = 0;
            return dmg;
        }

    }

    public partial class Warrior : Player{
        public override Type Class {
            get{
                return this.GetType();
            }
        }

        public Warrior(string name): base(name, 100){
            AddEncounterEffect(()=>{
                Buff resourceGen = new Buff("Warrior Resource", -1, null, this, true);
                resourceGen.AddActivation( new OnTryDefend ((hit, damage, attacker, Dtype, Atype) => {
                    Resource += damage / 2;
                    return hit;
                }));
                resourceGen.AddActivation( new AfterAttack( (damage, target, Dtype, Atype) => {
                    if(Atype == AttackType.AoE) Resource += damage / 4;
                    else Resource += damage / 2;
                }));
                addBuff(resourceGen);
            });
        }

        Buff ult;
        public override void activateClassAbility(){
            if(ClassAbilityActive) return;
            ClassAbilityActive = true;
            canTakeDamage = false;
            ult = new Buff("Endless Rage", -1, null, this);
            ult.AddActivation(new AfterTurn(()=> Resource -= 40));
            ult.locals.Add("Strength", 0);
            ult.AddActivation( new OnDamage( (damage) => {
                return 0;
            }));
            ult.AddActivation( new AfterDefend( (damage, attacker, Dtype, Atype) => {
                ult.locals["Strength"]++;
            }));
            addBuff(ult);
        }
        public override void endClassAbility(){
            if(ClassAbilityActive) {
                canTakeDamage = true;
                removeBuff(ult);
                ClassAbilityActive = false;
            }
        }

    } 
    public partial class Rogue : Player{
        public override Type Class {
            get{
                return this.GetType();
            }
        }
        public bool tookDouble = false;

        public Rogue(string name): base(name, 80){
            AddEncounterEffect(()=>{
                Buff resourceGen = new Buff("Rogue Resource", -1, null, this, true);

                resourceGen.AddActivation(new AfterUniqueLocalAdded((key, val, caster, target)=>{
                    if(key == "Poison" && caster == this) Resource += val;
                }));
                resourceGen.AddActivation(new AfterBuffApplied((b)=>{
                    if(b.Caster == this && b.BuffType == BuffClass.Trap){
                        b.AddActivation<OnTryDefend>(new OnTryDefend((hit, damage, attacker, Dtype, Atype)=>{
                            Resource += 5;
                            return hit;
                        }));
                    }
                }));
                resourceGen.AddActivation(new AfterAttack( (damage, target, Dtype, Atype) => {
                    if(Atype == AttackType.Daggers) Resource += damage;
                }));
                resourceGen.AddActivation(new AfterTryDefend( (damage, attacker, Dtype, Atype) => {
                    if(damage == null) Resource += 5;
                }));


                addBuff(resourceGen);
            });
        }

        Buff ult;
        public override void activateClassAbility(){
            if(ClassAbilityActive) return;
            ClassAbilityActive = true;
            tookDouble = false;
            DrawCards(1);
            ult = new Buff("Rogue Special", -1, null, this, true);
            ult.AddActivation(new AfterTurn(()=> Resource -= 40));
            ult.AddActivation(new BeforeTurn( () => {
                tookDouble = false;
                if(getTotalValue("Freeze") == 0) {
                    DrawCards(1);
                }
            }));

            addBuff(ult);
        }
        public override void endClassAbility(){
            if(ClassAbilityActive) {
                removeBuff(ult);
                ClassAbilityActive = false;
            }
        }

    } 
    public partial class Mage : Player{
        public override Type Class {
            get{
                return this.GetType();
            }
        }

        public Mage(string name): base(name, 80){

        }

        Buff ult;
        public override void activateClassAbility(){
            if(ClassAbilityActive) return;
            ClassAbilityActive = true;
            ult = new Buff("Mage Special", -1, null, this, true);
            ult.AddActivation(new AfterTurn(()=> Resource -= 40));

            //do shit here for ult effect

            addBuff(ult);
        }
        public override void endClassAbility(){
            if(ClassAbilityActive) {
                removeBuff(ult);
                ClassAbilityActive = false;
            }
        }

    }
    public partial class Cleric : Player{
        public override Type Class {
            get{
                return this.GetType();
            }
        }

        public Cleric(string name): base(name, 100){
            AddEncounterEffect(()=>{
                Buff resourceGen = new Buff("Cleric Resource", -1, null, this, true);
                
                resourceGen.AddActivation(new AfterBuffApplied((b)=>{
                    if(b.Caster == this && b.locals.ContainsKey("Shield")){
                        b.AddActivation(new AfterLocalChanged((key, val1, val2)=>{
                            if(key == "Shield" && val1 > val2)
                                Resource += (val1 - val2);
                        }));
                    }
                }));
                resourceGen.AddActivation( new AfterApplyHeal( (heal, target) => {
                    Resource += heal;
                }));
                resourceGen.AddActivation( new AfterAttack( (damage, target, Dtype, Atype) => {
                    if(Atype == AttackType.Smite) Resource += damage;
                }));

                addBuff(resourceGen);
            });
        }

        Buff ult;
        public override void activateClassAbility(){
            if(ClassAbilityActive) return;
            ClassAbilityActive = true;

            ult = new Buff("Cleric Special", -1, null, this, true);
            ult.AddActivation(new AfterTurn(()=> Resource -= 40));

            List<Buff> strBuffs= new List<Buff>();
            List<Buff> intBuffs= new List<Buff>();
            List<Buff> regBuffs= new List<Buff>();
            int duration = 1;

            foreach(Player p in Lobby.players.Values){
                strBuffs.Add(p.addStatBuff("Strength", 2, -1, this));
                intBuffs.Add(p.addStatBuff("Intellect", 2, -1, this));
                regBuffs.Add(p.addStatBuff("Regen", 2, -1, this));
            }

            ult.AddActivation( new BeforeTurn( () => {
                foreach(Buff b in strBuffs)
                    b.locals["Strength"]+=2;
                foreach(Buff b in intBuffs)
                    b.locals["Intellect"]+=2;
                foreach(Buff b in regBuffs)
                    b.locals["Regen"]+=2;
                duration++;
            }));
            ult.AddActivation( new OnExpire( () => {
                foreach(Buff b in strBuffs){ 
                    b.Duration = duration;
                    b.AddDecay();
                }
                foreach(Buff b in intBuffs){
                    b.Duration = duration;
                    b.AddDecay();
                }
                foreach(Buff b in regBuffs){
                    b.Duration = duration;
                    b.AddDecay();
                }
            }));

            addBuff(ult);
            Lobby.updateUI();
        }
        public override void endClassAbility(){
            if(ClassAbilityActive) {
                removeBuff(ult);
                ClassAbilityActive = false;
            }
        }

    } 
    /*public partial class template : Player{
        public override PlayerClass Class {
            get{
                return PlayerClass.template;
            }
        }

        public template(string name): base(name, MAXHEALTH)
            AddEncounterEffect(()=>{
                Buff resourceGen = new Buff("template Resource", -1, -1, this, true);
                
                //do shit here for resource gen

                addBuff(resourceGen);
            });
        }

        Buff ult;
        public override void activateClassAbility(){
            if(ClassAbilityActive) return;
            ult = new Buff("Endless Rage", -1, -2, this);

            //do shit here for ult effect

            addBuff(ult);
            ClassAbilityActive = true;
        }
        public override void endClassAbility(){
            if(ClassAbilityActive) {
                removeBuff(ult);
                ClassAbilityActive = false;
            }
        }

    } */

    public abstract class SkillTree{
        public static int countActive(SkillNode[] nodes){ // section, row, collumn
            int sum = 0;
            foreach(SkillNode s in nodes)
                if(s.Status == NodeStatus.Activated) sum++;
            return sum;
        }

        private Player self;
        public Player Self{
            get{
                return self;
            }
        }
        protected SkillNode[][][] nodes;

        public SkillNode this[int i, int j, int k]{
            get{
                return nodes[i][j][k];
            }
        }
        public SkillNode[] this[int i, int j]{
            get{
                return nodes[i][j];
            }
        }

        public abstract bool pickNode(int i, int j, int k);
        public abstract string[] availibleNodes();
        public abstract int cardRemovalNode(int i, int j, int k);

        public SkillTree(Player self, Action[][][] skillTreeEffects){
            this.self = self;
            nodes = new SkillNode[skillTreeEffects.Length][][];
            for(int i = 0; i < skillTreeEffects.Length; i++){
                nodes[i] = new SkillNode[skillTreeEffects[i].Length][];
                for(int j = 0; j < skillTreeEffects[i].Length; j++){
                    nodes[i][j] = new SkillNode[skillTreeEffects[i][j].Length];
                    for(int k = 0; k < skillTreeEffects[i][j].Length; k++){
                        nodes[i][j][k] = new SkillNode(skillTreeEffects[i][j][k]);
                    }
                }
            }
            for(int i = 0; i < nodes[0][0].Length; i++)
                nodes[0][0][i] = new SkillNode(nodes[0][0][i].Effect, NodeStatus.Unlocked);
        } 
    }
    public class StandardSkillTree : SkillTree{
        const SectionStatus l = SectionStatus.Locked; // section, row, collumn
        private Tuple<int, int>[][] collunmRestrictions; // min, max
        private SectionStatus[] sectionStatus = new SectionStatus[7]{SectionStatus.Unlocked, l, l, l, l, l, l};

        public StandardSkillTree(Player self, Action[][][] skillTreeEffects) : base(self, skillTreeEffects){
            Tuple<int, int> a = new Tuple<int, int>(2,3);
            Tuple<int, int>[] b = new Tuple<int, int>[] {a, a, a, new Tuple<int, int>(1,1)};
            collunmRestrictions = new Tuple<int, int>[][] {b, b, b, b, b, b, b};
        }

        private void unlockSection(int s){
            sectionStatus[s] = SectionStatus.Unlocked;
            for(int i = 0; i < nodes[s][0].Length; i++)
                nodes[s][0][i] = new SkillNode(nodes[s][0][i].Effect, NodeStatus.Unlocked);
        }

        public override bool pickNode(int i, int j, int k){
            if(sectionStatus[i] == SectionStatus.Locked) return false;
            else if(nodes[i][j][k].Status == NodeStatus.Locked) return false;
            else if(nodes.Length < i) return false;
            else if(nodes[i].Length < j) return false;
            else if(nodes[i][j].Length < k) return false;
            else{
                nodes[i][j][k].Effect();
                nodes[i][j][k] = new SkillNode(nodes[i][j][k].Effect, NodeStatus.Activated);
                if(countActive(nodes[i][j]) == collunmRestrictions[i][j].Item2) //hit row maximum
                    for(int x = 0; x < nodes[i][j].Length; x++)
                        if(nodes[i][j][x].Status != NodeStatus.Activated) 
                            nodes[i][j][x] = new SkillNode(nodes[i][j][x].Effect, NodeStatus.Locked);
                if(countActive(nodes[i][j]) == collunmRestrictions[i][j].Item1){ //hit row minimum
                    if(nodes[i].Length == j+1){ //hit end of section
                        if(i == 0){ //NOTE: temporarily disabled because of lack of content
                            for(int x = 1; x <= 3; x++)
                                ;//unlockSection(x);
                        }
                        else if(i < 4){
                            ;//unlockSection(i+3);
                        }
                    }
                    else{
                        for(int x = 0; x < nodes[i][j+1].Length; x++)
                            nodes[i][j+1][x] = new SkillNode(nodes[i][j+1][x].Effect, NodeStatus.Unlocked);
                    }
                }
                if(sectionStatus[i] == SectionStatus.Unlocked){
                    if((i == 4 || i == 5 || i == 6) && !advanced){
                        sectionStatus[4] = SectionStatus.Locked;
                        sectionStatus[5] = SectionStatus.Locked;
                        sectionStatus[6] = SectionStatus.Locked;
                        advanced = true;
                    }
                    sectionStatus[i] = SectionStatus.Activated;
                    if(i == 1 || i == 2 || i == 3){
                        if(sectionStatus[1] != SectionStatus.Unlocked && sectionStatus[2] != SectionStatus.Unlocked) sectionStatus[3] = SectionStatus.Locked;
                        if(sectionStatus[1] != SectionStatus.Unlocked && sectionStatus[3] != SectionStatus.Unlocked) sectionStatus[2] = SectionStatus.Locked;
                        if(sectionStatus[2] != SectionStatus.Unlocked && sectionStatus[3] != SectionStatus.Unlocked) sectionStatus[1] = SectionStatus.Locked;
                    }
                }
                return true;
            }
        }
        bool advanced = false;
        
        public override string[] availibleNodes(){
            List<string> keys = new List<string>();
            for(int i = 0; i < nodes.Length; i++){
                if(sectionStatus[i] == SectionStatus.Locked) continue;
                for(int j = 0; j < nodes[i].Length; j++)
                    for(int k = 0; k < nodes[i][j].Length; k++)
                        if(nodes[i][j][k].Status == NodeStatus.Unlocked)
                            keys.Add((i+"")+(j+"")+(k+""));
            }
            return keys.ToArray();
        }
        
        public override int cardRemovalNode(int i, int j, int k){
            try{
                if(j == 1 && k == 3){
                    switch(i){
                        case 0: return 1;
                        case 1: case 2: case 3: return 2;
                        default: return 3;
                    }
                }
                return 0;
            }
            catch {
                return 0;
            }
        }
    }
    public class SkillNode{
        private NodeStatus activated = NodeStatus.Locked;
        public NodeStatus Status{
            get{
                return activated;
            }
        }
        private Action effect;
        public Action Effect{
            get{
                return effect;
            }
        }

        public SkillNode(Action effect){
            this.effect = effect;
        }

        public SkillNode(Action effect, NodeStatus activated){
            this.effect = effect;
            this.activated = activated;
        }
    }

    public partial class Card{
        public override string ToString() {return name;}
        private Player self = null;
        public Player Self{
            get{
                return self;
            }
        }
        private string id;
        public string Id{
            get{
                return id;
            }
        }
        public void setSelf(Player self, string id){
            if(this.self == null && self != null){
                this.self = self;
                this.id = id;
            }
        }
        CardTargeting? targeting; //e:enemy, a:ally, p:player, n:any
        public CardTargeting? Targeting{
            get{
                return targeting;
            }
        }
        string name;
        public string Name{
            get{
                return name;
            }
        }
        Action<Player, Entity, Card> effect;
        public Action<Player, Entity, Card> Effect{
            get{
                return effect;
            }
        }
        public CardLocals locals;
        private Action<Entity, Card> onEncounterStart = null;
        public Action<Entity, Card> OnEncounterStart{
            get{
                return onEncounterStart;
            }
        }
        private Action<Entity, Card> onEncounterEnd = null;
        public Action<Entity, Card> OnEncounterEnd{
            get{
                return onEncounterEnd;
            }
        }
        public Card(string name, CardTargeting? targeting, Action<Player, Entity, Card> effect){
            this.name = name;
            this.targeting = targeting;
            this.effect = effect;
            locals = new CardLocals(this);
        }
        public Card(string name, CardTargeting? targeting, Action<Player, Entity, Card> effect, Action<Entity, Card> startEncounter): this(name, targeting, effect){
            onEncounterStart = startEncounter;
        }
        public Card(string name, CardTargeting? targeting, Action<Player, Entity, Card> effect, Action<Entity, Card> startEncounter, Action<Entity, Card> endEncounter): this(name, targeting, effect, startEncounter){
            onEncounterEnd = endEncounter;
        }

    }
    public class CardLocals{
        private Dictionary<string, int> locals = new Dictionary<string, int>();
        public Dictionary<string, int> Dict {
            get{
                Dictionary<string, int> d = new Dictionary<string, int>();
                foreach(string k in locals.Keys) d.Add(k, locals[k]);
                return d;
            }
        }
        private Card self;
        public Dictionary<string, int>.KeyCollection Keys{
            get{
                return locals.Keys;
            }
        }
        public Dictionary<string, int>.ValueCollection Values{
            get{
                return locals.Values;
            }
        }

        public CardLocals(Card self){
            this.self = self;
        }

        public int this[string key]{
            get{
                return locals[key];
            }
            set{
                //if(!canChange) return;
                int v1 = locals[key];
                locals[key] = value;
                Lobby.cardLocalChanged(self);
            }
        }
        public void Add(string key, int value){
            if(locals.ContainsKey(key)) throw new ArgumentException("Key \""+key+"\" already exists in local dictionary");
            else{
                locals.Add(key, value);
                Lobby.cardLocalChanged(self);
            }
        }
        public bool ContainsKey(string key){
            return locals.ContainsKey(key);
        }
        //public bool canChange = true;
        
    }
    public class Intention{
        int damage;
        public int Damage {
            get{
                return damage;
            }
        }
        DamageType? damageType;
        public DamageType? DamageType {
            get{
                return damageType;
            }
        }
        int hitCount;
        public int HitCount {
            get{
                return hitCount;
            }
        }
        bool[] actionType; //bool array: debuff, buff, utility
        public bool[] ActionType {
            get{
                return actionType;
            }
        }
        Action<Enemy, Player> behavior;
        public Action<Enemy, Player> Behavior {
            get{
                return behavior;
            }
        }
        public string[] targets;
        private int tars;
        public int Targets{
            get{
                return tars;
            }
        }
        public Intention(int damage, DamageType? damageType, int targets, int hitcount, bool[] actionType, Action<Enemy, Player> behavior){
            this.damage = damage;
            this.damageType = damageType;
            this.actionType = actionType;
            this.behavior = behavior;
            this.tars = targets;
            if(targets >= 0) this.targets = new string[targets];
            else this.targets = new string[0];
            this.hitCount = hitcount;
        }
    }

    public class Buff{
        Dictionary<Type, BuffActivation> effects  = new Dictionary<Type, BuffActivation>();
        public Dictionary<Type, BuffActivation>.ValueCollection buffActivations{
            get{
                return effects.Values;
            }
        }
        public override string ToString() {return name + " - " + id;}
        string name;
        public string Name {
            get{
                return name;
            }
        }
        string id = null;
        public string Id{
            get{
                return id;
            }
        }
        bool hidden = false;
        public bool Hidden{
            get{
                return hidden;
            }
            set{
                if(value != hidden){
                    if(value){
                        Lobby.buffExpire(this);
                        hidden = value;
                    }
                    else{
                        hidden = value;
                        Lobby.buffAdded(this);
                    }
                }
            }
        }
        int duration; 
        Type durationType;
        public Type DurationType{
            get{
                return durationType;
            }
        }
        public int Duration {
            get{
                return duration;
            }
            set{
                int prev = duration;
                duration = value;
                GetActivation<OnDurationChange>().D(prev);
                if(self != null){
                    if(value <= 0)
                        self.removeBuff(this);
                    else Lobby.buffDurationChanged(this);
                }
            }
        }
        public BuffLocals locals;
        Entity self = null;
        public Entity Self {
            get{
                return self;
            }
        }
        Entity caster;
        public Entity Caster{
            get{
                return caster;
            }
        }
        BuffClass buffType = BuffClass.Misc;
        public BuffClass BuffType{
            get{
                return buffType;
            }
        }
        public Buff(string name, int duration, Type durationType, Entity castor, bool hidden):this(name, duration, durationType, castor){
            this.hidden = hidden;
        }
        public Buff(string name, int duration, Type durationType, Entity castor, BuffClass buffType):this(name, duration, durationType, castor){
            this.buffType = buffType;
        }
        public Buff(string name, int duration, Type durationType, Entity castor){
            this.name = name;
            this.duration = duration;
            this.durationType = durationType;
            this.caster = castor;
            locals = new BuffLocals(this);
        }

        public E GetActivation<E>() where E : BuffActivation, new(){
            if(effects != null && effects.ContainsKey(typeof(E)) && self.Alive) return (E)effects[typeof(E)];
            else{
                E e = new E();
                e.setHost(this);
                return e;
            } 
        }

        public void nullify(){
            duration = 0;
            effects = null;
        }

        public void AddEffect(string key){
            Entity.addEffect(this, key);
        }
        public void AddActivation<E>(E e) where E : BuffActivation{
            if(effects == null) return;
            e.setHost(this);
            if(effects.ContainsKey(typeof(E)))
                effects[typeof(E)].Combine(e);
            else effects.Add(typeof(E), e);
        }
        public Buff AddDecay(){
            foreach(string k in locals.Keys)
                if(Entity.StatBuffExists(k))
                    AddDecay(k);
            return this;
        }
        private List<string> decays = new List<string>();
        public string[] Decays{
            get{
                return decays.ToArray();
            }
        } 
        public Buff AddDecay(string k){
            if(effects == null) return null;
            if(!Entity.StatBuffExists(k)) throw new ArgumentException("Stat buff key \""+k+"\" is invalid");
            if(!locals.ContainsKey(k)) throw new ArgumentException("Buff does not contain stat key \""+k+"\"");
            if(decays.Contains(k)) return this;
            float value = locals[k];
            bool canChange = true;
            AddActivation(new OnDurationChange((prev)=>{
                if(prev > Duration){
                    canChange = false;
                    float frac = ((float)Duration) / ((float)prev);
                    value *= frac;
                    locals[k] = (int)Math.Round(value);
                    canChange = true;
                }
            }));
            AddActivation(new AfterLocalChanged((key, prev, val)=>{
                if(key == k && canChange)
                    value += val - prev;
            }));
            decays.Add(k);
            if(self != null) Lobby.buffLocalDecayChanged(this, k);
            return this;
        }
        public void setHost(Entity self){
            if(this.self == null) this.self = self;
        }
        public void setID(string id){
            if(this.id == null) this.id = id;
        }
        public void Remove(){
            self.removeBuff(this);
        }

    }
    public class BuffLocals{
        private Dictionary<string, int> locals = new Dictionary<string, int>();
        public Dictionary<string, int> Dict {
            get{
                Dictionary<string, int> d = new Dictionary<string, int>();
                foreach(string k in locals.Keys) d.Add(k, locals[k]);
                return d;
            }
        }
        private Buff self;
        public Dictionary<string, int>.KeyCollection Keys{
            get{
                return locals.Keys;
            }
        }
        public Dictionary<string, int>.ValueCollection Values{
            get{
                return locals.Values;
            }
        }

        public BuffLocals(Buff self){
            this.self = self;
        }

        public int this[string key]{
            get{
                return locals[key];
            }
            set{
                if(!canChange) return;
                int v1 = locals[key];
                locals[key] = value;
                if(self.Self != null) Lobby.buffLocalChanged(this.self, key);
                self.GetActivation<AfterLocalChanged>().D(key, v1, value);
                Enemy e;
                if((e = Lobby.getEnemyFromID(self.Self.Id)) != null && (key == "Intellect" || key == "Strength") && v1 != value){
                    Lobby.attackSeverityChanged(e);
                }
            }
        }
        public void Add(string key, int value){
            if(locals.ContainsKey(key)) throw new ArgumentException("Key \""+key+"\" already exists in local dictionary");
            else{
                locals.Add(key, value);
                if(self.Self != null) Lobby.buffLocalChanged(this.self, key);
                //self.GetActivation<AfterLocalChanged>().D(key, null, value);
            }
        }
        public bool ContainsKey(string key){
            return locals.ContainsKey(key);
        }
        public bool canChange = true;
        
    }
    public abstract class BuffActivation {
        protected Buff buff = null;

        public void setHost(Buff b){
            if(buff == null) buff = b;
        }

        protected E callEffect<E>(Func<E> callback, E defalt){
            if(buff == null) throw new NullReferenceException();

            if(buff.Duration == 0) return defalt;
            if(buff.Self == null) return defalt;
        
            if(!buff.Self.Alive){
                buff.Self.removeBuff(buff);
                return defalt;
            }

            E ret = defalt;
            if(callback != null){
                ret = callback();
                //Lobby.buffActive(buff);
            }
            
            if(buff.DurationType == this.GetType() && buff.Duration != -1) buff.Duration--;

            return ret;
        }

        public void callEffect(Action callback){
            if(buff == null) throw new NullReferenceException();

            if(buff.Duration == 0) return;
            if(buff.Self == null) return;
        
            if(!buff.Self.Alive){
                return;
            }

            if(callback != null){
                callback();
                //Lobby.buffActive(buff);
            }

            if(buff.DurationType == this.GetType() && buff.Duration != -1) buff.Duration--;
        }

        public abstract void Combine<E>(E b) where E : BuffActivation;

    }
    #region buff activations
    public class StartRound : BuffActivation{
        private Action d = null;

        public StartRound(){}
        public StartRound(Action d){
            this.d = d;
        }

        public void D(){
            callEffect(d);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((StartRound)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }
    }
    public class BeforeTurn : BuffActivation{
        private Action d = null;

        public BeforeTurn(){}
        public BeforeTurn(Action d){
            this.d = d;
        }

        public void D(){
            callEffect(d);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((BeforeTurn)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterTurn : BuffActivation{
        private Action d = null;

        public AfterTurn(){}
        public AfterTurn(Action d){
            this.d = d;
        }

        public void D(){
            callEffect(d);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterTurn)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterCastorTurn : BuffActivation{
        private Action d = null;

        public AfterCastorTurn(){}
        public AfterCastorTurn(Action d){
            this.d = d;
        }

        public void D(){
            callEffect(d);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterCastorTurn)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class TurnTaken : BuffActivation{
        private Action d = null;

        public TurnTaken(){}
        public TurnTaken(Action d){
            this.d = d;
        }

        public void D(){
            callEffect(d);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((TurnTaken)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class EndRound : BuffActivation{
        private Action d = null;

        public EndRound(){}
        public EndRound(Action d){
            this.d = d;
        }

        public void D(){
            callEffect(d);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((EndRound)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnTryAttack : BuffActivation{
        private Func<bool, int, Entity, DamageType, AttackType, bool> d = null;

        public OnTryAttack(){}
        public OnTryAttack(Func<bool, int, Entity, DamageType, AttackType, bool> d){
            this.d = d;
        }

        public bool D(bool hit, int damage, Entity target, DamageType Dtype, AttackType Atype){
            Func<bool> a = null;
            return callEffect(d==null?a:()=>d(hit, damage, target, Dtype, Atype), hit);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnTryAttack)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnTryDefend : BuffActivation{
        private Func<bool, int, Entity, DamageType, AttackType, bool> d = null;

        public OnTryDefend(){}
        public OnTryDefend(Func<bool, int, Entity, DamageType, AttackType, bool> d){
            this.d = d;
        }

        public bool D(bool hit, int damage, Entity attacker, DamageType Dtype, AttackType Atype){
            Func<bool> a = null;
            return callEffect(d==null?a:()=>d(hit, damage, attacker, Dtype, Atype), hit);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnTryDefend)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnAttack : BuffActivation{
        private Func<int, Entity, DamageType, AttackType, int> d = null;

        public OnAttack(){}
        public OnAttack(Func<int, Entity, DamageType, AttackType, int> d){
            this.d = d;
        }

        public int D(int damage, Entity target, DamageType Dtype, AttackType Atype){
            Func<int> a = null;
            return callEffect(d==null?a:()=>d(damage, target, Dtype, Atype), damage);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnAttack)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnDefend : BuffActivation{
        private Func<int, Entity, DamageType, AttackType, int> d = null;

        public OnDefend(){}
        public OnDefend(Func<int, Entity, DamageType, AttackType, int> d){
            this.d = d;
        }

        public int D(int damage, Entity attacker, DamageType Dtype, AttackType Atype){
            Func<int> a = null;
            return callEffect(d==null?a:()=>d(damage, attacker, Dtype, Atype), damage);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnDefend)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnDamage : BuffActivation{
        private Func<int, int> d = null;

        public OnDamage(){}
        public OnDamage(Func<int, int> d){
            this.d = d;
        }

        public int D(int damage){
            Func<int> a = null;
            return callEffect(d==null?a:()=>d(damage), damage);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnDamage)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterDamage : BuffActivation{
        private Action<int> d = null;

        public AfterDamage(){}
        public AfterDamage(Action<int> d){
            this.d = d;
        }

        public void D(int damage){
            Action a = null;
            callEffect(d==null?a:()=>d(damage));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterDamage)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterDefend : BuffActivation{
        private Action<int, Entity, DamageType, AttackType> d = null;

        public AfterDefend(){}
        public AfterDefend(Action<int, Entity, DamageType, AttackType> d){
            this.d = d;
        }

        public void D(int damage, Entity attacker, DamageType Dtype, AttackType Atype){
            Action a = null;
            callEffect(d==null?a:()=>d(damage, attacker, Dtype, Atype));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterDefend)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterAttack : BuffActivation{
        private Action<int, Entity, DamageType, AttackType> d = null;

        public AfterAttack(){}
        public AfterAttack(Action<int, Entity, DamageType, AttackType> d){
            this.d = d;
        }

        public void D(int damage, Entity target, DamageType Dtype, AttackType Atype){
            Action a = null;
            callEffect(d==null?a:()=>d(damage, target, Dtype, Atype));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterAttack)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterTryDefend : BuffActivation{
        private Action<int?, Entity, DamageType, AttackType> d = null;

        public AfterTryDefend(){}
        public AfterTryDefend(Action<int?, Entity, DamageType, AttackType> d){
            this.d = d;
        }

        public void D(int? damage, Entity attacker, DamageType Dtype, AttackType Atype){
            Action a = null;
            callEffect(d==null?a:()=>d(damage, attacker, Dtype, Atype));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterTryDefend)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterTryAttack : BuffActivation{
        private Action<int?, Entity, DamageType, AttackType> d = null;

        public AfterTryAttack(){}
        public AfterTryAttack(Action<int?, Entity, DamageType, AttackType> d){
            this.d = d;
        }

        public void D(int? damage, Entity target, DamageType Dtype, AttackType Atype){
            Action a = null;
            callEffect(d==null?a:()=>d(damage, target, Dtype, Atype));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterTryAttack)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterAoE : BuffActivation{
        private Action<int, int, DamageType, bool> d = null;

        public AfterAoE(){}
        public AfterAoE(Action<int, int, DamageType, bool> d){
            this.d = d;
        }

        public void D(int damage, int hits, DamageType Dtype, bool burst){
            Action a = null;
            callEffect(d==null?a:()=>d(damage, hits, Dtype, burst));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterAoE)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnApplyHeal : BuffActivation{
        private Func<int, Entity, int> d = null;

        public OnApplyHeal(){}
        public OnApplyHeal(Func<int, Entity, int> d){
            this.d = d;
        }

        public int D(int heal, Entity target){
            Func<int> a = null;
            return callEffect(d==null?a:()=>d(heal, target), heal);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnApplyHeal)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnHeal : BuffActivation{
        private Func<int, Entity, int> d = null;

        public OnHeal(){}
        public OnHeal(Func<int, Entity, int> d){
            this.d = d;
        }

        public int D(int heal, Entity castor){
            Func<int> a = null;
            return callEffect(d==null?a:()=>d(heal, castor), heal);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnHeal)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterHeal : BuffActivation{
        private Action<int, Entity> d = null;

        public AfterHeal(){}
        public AfterHeal(Action<int, Entity> d){
            this.d = d;
        }

        public void D(int heal, Entity castor){
            Action a = null;
            callEffect(d==null?a:()=>d(heal, castor));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterHeal)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterApplyHeal : BuffActivation{
        private Action<int, Entity> d = null;

        public AfterApplyHeal(){}
        public AfterApplyHeal(Action<int, Entity> d){
            this.d = d;
        }

        public void D(int heal, Entity target){
            Action a = null;
            callEffect(d==null?a:()=>d(heal, target));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterApplyHeal)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterBuffApplied : BuffActivation{
        private Action<Buff> d = null;

        public AfterBuffApplied(){}
        public AfterBuffApplied(Action<Buff> d){
            this.d = d;
        }

        public void D(Buff buff){
            Action a = null;
            callEffect(d==null?a:()=>d(buff));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterBuffApplied)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterLocalChanged : BuffActivation{
        private Action<string, int, int> d = null;

        public AfterLocalChanged(){}
        public AfterLocalChanged(Action<string, int, int> d){
            this.d = d;
        }

        public void D(string key, int val1, int val2){
            Action a = null;
            callEffect(d==null?a:()=>d(key, val1, val2));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterLocalChanged)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class AfterUniqueLocalAdded : BuffActivation{
        private Action<string, int, Entity, Entity> d = null;

        public AfterUniqueLocalAdded(){}
        public AfterUniqueLocalAdded(Action<string, int, Entity, Entity> d){
            this.d = d;
        }

        public void D(string key, int val1, Entity caster, Entity target){
            Action a = null;
            callEffect(d==null?a:()=>d(key, val1, caster, target));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterUniqueLocalAdded)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnResourceChange : BuffActivation{
        private Func<int, int> d = null;

        public OnResourceChange(){}
        public OnResourceChange(Func<int, int> d){
            this.d = d;
        }

        public int D(int res){
            Func<int> a = null;
            return callEffect(d==null?a:()=>d(res), res);
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnResourceChange)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }

    }
    public class OnExpire : BuffActivation{
        private Action d = null;

        public OnExpire(){}
        public OnExpire(Action d){
            this.d = d;
        }

        public void D(){
            Action a = null;
            callEffect(d==null?a:()=>d());
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnExpire)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }
    }
    public class OnDurationChange : BuffActivation{
        private Action<int> d = null;

        public OnDurationChange(){}
        public OnDurationChange(Action<int> d){
            this.d = d;
        }

        public void D(int prev){
            Action a = null;
            callEffect(d==null?a:()=>d(prev));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((OnDurationChange)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }
    }
    public class AfterHealthChanged : BuffActivation{
        private Action<int> d = null;

        public AfterHealthChanged(){}
        public AfterHealthChanged(Action<int> d){
            this.d = d;
        }

        public void D(int prev){
            Action a = null;
            callEffect(d==null?a:()=>d(prev));
        }
        public override void Combine<E>(E b){
            if(typeof(E) == this.GetType())
                d += ((AfterHealthChanged)((object)b)).d;
            else throw new NotSupportedException("Type \""+typeof(E)+"\" cannot be combined with type \""+this.GetType()+"\"");
        }
    }
    #endregion

    #region enums
    public enum BuffClass {Misc, Trap, Hex};
    public enum CardTargeting {Any, Ally, Player, Enemy}
    public enum DamageType {Physical, Magic};
    public enum AttackType {Base, AoE, Daggers, Smite, Burst};
    public enum PlayerStatus {SkillTree, CardPool, Ready};
    public enum SectionStatus {Locked, Unlocked, Activated, Completed};
    public enum NodeStatus {Locked, Unlocked, Activated};
    #endregion

    public class LobbyCommand{
        Action<string, string[]> action;
        string name;
        public string Name{
            get{
                return name;
            }
        }
        string description;
        public string Description{
            get{
                return description;
            }
        }

        public LobbyCommand(string name, string description, Action<string, string[]> action){
            if(name.Contains(" ")) throw new ArgumentException("Lobby command name \""+name+"\" is an invalid name");
            this.name = name;
            this.description = description;
            this.action = action;
        }

        public void Action(string user, string[] args){
            action(user, args);
        }
    }
    public class EncounterEndException : Exception{
        bool win;
        public bool Win{
            get{
                return win;
            }
        }

        public EncounterEndException(bool w){
            win = w;
        }

        public override string ToString(){
            return "EncounterEndException: The encounter was ended with win condition "+win+"\n"+base.StackTrace;
        }
    }
}