using System.Collections.Generic;
using System.Collections;
using System;
using classes;

namespace classes {
    public partial class Card {
        const DamageType phs = DamageType.Physical;
        const DamageType mag = DamageType.Magic;
        const CardTargeting any = CardTargeting.Any;
        const CardTargeting ally = CardTargeting.Ally;
        const CardTargeting enemy = CardTargeting.Enemy;
        const CardTargeting player = CardTargeting.Player;

        public static Card[] GetCards(string[] cardNames){
            Card[] cards = new Card[cardNames.Length];
            for(int i = 0; i < cardNames.Length; i++)
                cards[i] = GetCard(cardNames[i]);
            return cards;
        }

        public static bool CardExists(string cardName){
            try{
                GetCard(cardName);
                return true;
            }
            catch{
                return false;
            }
        }

        public static Card GetCard(string cardName){
            switch(cardName){

                #region warrior cards

                #region starter
                case "Strike": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(10, cardTarget, phs); 
                });
                case "Stand Ground": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Shield", 15, 2, self);
                });
                case "Bash": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(10, cardTarget, phs);
                    cardTarget.addStatBuff("Armor", -2, 2, self);
                });
                case "Double Strike": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(6, cardTarget, phs);
                    self.Attack(6, cardTarget, phs);
                });
                case "Strengthen": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Strength", 3, 6, self);
                });
                case "Toughen": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Armor", 2, 6, self);
                    self.addStatBuff("Resolve", 2, 6, self);
                });
                #endregion
                
                #region beginner
                case "Cleave": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.AttackAoE(8, phs);
                });
                case "Reckless Strike": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(25, cardTarget, phs);
                    self.addStatBuff("Armor", -5, 1, self);
                    self.addStatBuff("Resolve", -5, 1, self);
                });
                case "Frenzied Strikes": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(4, cardTarget, phs);
                    self.Attack(4, cardTarget, phs);
                    if(self.Health <= self.MaxHealth * 3f / 4f) self.Attack(4, cardTarget, phs);
                    if(self.Health <= self.MaxHealth / 2f) self.Attack(4, cardTarget, phs);
                    if(self.Health <= self.MaxHealth / 4f) self.Attack(4, cardTarget, phs);
                });
                case "Strengthened Strike": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(15 + (2 * self.getTotalValue("Strength")), cardTarget, phs);
                });
                case "Scarred Strength": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.Health -= 5;
                    self.addStatBuff("Strength", 3, -1, self);
                });
                

                case "Defensive Strike": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.addStatBuff("Shield", 5 + self.AttackI(10, cardTarget, phs), 2, self);
                });
                case "Swordbreaker": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(10, cardTarget, phs);
                    cardTarget.addStatBuff("Strength", -3, 3, self);
                });
                case "Sanguine Strike": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Heal(self.AttackI(10, cardTarget, phs) / 2, self);
                });
                case "Against the Odds": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Taunt(Lobby.getEnemyFromID(cardTarget.Id));
                    int attackers = 0;
                    foreach(Enemy e in Lobby.enemies) 
                        if(e.PlannedAction.Targets == -1 || Array.IndexOf(e.PlannedAction.targets, self) != -1)
                            attackers++;
                    self.addStatBuff("Strength", attackers, 2, self);
                });
                case "Last Stand": return new Card(cardName, null, (self, cardTarget, card) => {
                    float hp = (float)self.Health / (float)self.MaxHealth;
                    int regen = 1;
                    if(hp <= .25) regen = 4;
                    else if(hp <= .50) regen = 3;
                    else if(hp <= .75) regen = 2;
                    self.addStatBuff("Regen", regen, 5, self);

                });


                case "Insulting Presence": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff b = new Buff(cardName, 4, typeof(AfterTurn), self);
                    b.AddActivation(new AfterTurn(() => {
                        Lobby.getPlayerFromID(b.Self.Id).Taunt(Lobby.getRandomEnemy());
                    }));
                    self.addBuff(b);
                });
                case "Defensive Taunt": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Taunt(Lobby.getEnemyFromID(cardTarget.Id));
                    self.addStatBuff("Shield", 15, 1, self);
                });
                case "Unbreakable": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Shield", 10, -1, self);
                    self.addStatBuff("Armor", 1, -1, self);
                    self.addStatBuff("Resolve", 1, -1, self);
                });
                case "Bulwark": return new Card(cardName, null, (self, cardTarget, card) => {
                    foreach(Buff b in self.buffs.Values) 
                        if(b.locals.ContainsKey("Shield") && b.Duration != -1) 
                            b.Duration += 2;
                    self.addStatBuff("Shield", 15, 3, self);
                });
                case "Blood Armor": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff b = self.addStatBuff("Blood Armor", "Shield", 15, 1, self);
                    b.AddActivation(new OnExpire(()=>{
                        if(b.locals["Shield"] != 0)
                            b.Self.addStatBuff("Regen", 1, b.locals["Shield"], b.Self);
                    }));
                });


                case "Flurry of Blows": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    for(int i = 0; i < card.locals["x"]; i++)
                        self.Attack(5, cardTarget, phs);
                    card.locals["x"]++;
                }, (self, card)=>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 3);
                    else card.locals["x"] = 3;
                });
                case "Culling": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(10, cardTarget, phs);
                    if(!cardTarget.Alive)
                        self.MaxHealth += 5;
                });
                case "Barricade": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff b = self.addStatBuff("Shield", card.locals["x"], 1, self);
                    b.AddActivation(new AfterLocalChanged((key, val1, val2) => {
                        if(key == "Shield")
                            card.locals["x"] = (b.locals["Shield"] <= 15 ? 15 : b.locals["Shield"]);
                    }));
                }, (self, card)=>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 60);
                    else card.locals["x"] = 60;
                });
                #endregion
                
                #endregion
                #region rogue cards

                #region starter
                case "Double Slash": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(5, cardTarget, phs);
                    self.Attack(5, cardTarget, phs);
                });
                case "Sneak": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Dodge", 2, 2, self);
                });
                case "Acidic Poison": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    cardTarget.addUniqueLocal("Poison", 4, self);
                    cardTarget.addStatBuff("Strength", -2, 2, self);
                    cardTarget.addStatBuff("Armor", -2, 2, self);
                });
                case "Tripwire": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addTrapBuff(cardName, 3, self, (damage, attacker, Dtype, Atype)=>{
                        attacker.addUniqueLocal("Stun", 1, self);
                    });
                });
                case "Flurry Slash": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(3, cardTarget, phs);
                    self.Attack(3, cardTarget, phs);
                    self.Attack(3, cardTarget, phs);
                    self.Attack(3, cardTarget, phs);
                });
                case "Preparation": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addUniqueLocal("Daggers", 5, self);
                });
                #endregion

                #region beginner
                case "Cloak and Dagger": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Dodge", 2, 2, self);
                    self.addUniqueLocal("Daggers", 3, self);
                });
                case "Deadly Acrobat": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff b = new Buff(cardName, -1, null, self);
                    b.AddActivation(new AfterTryDefend( (damage, target, Dtype, Atype) => {
                        if(damage == null) self.addUniqueLocal("Daggers", 2, self);
                    }));
                    self.addBuff(b);
                });
                case "Sharpen": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff b = new Buff(cardName, -1, null, self);
                    b.AddActivation(new OnAttack( (damage, target, Dtype, Atype) => {
                        if(Atype == AttackType.Daggers) return damage + 2;
                        else return damage;
                    }));
                    self.addBuff(b);
                });
                case "Poison Daggers": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff b = new Buff(cardName, -1, null, self);
                    b.AddActivation(new OnAttack( (damage, target, Dtype, Atype) => {
                        if(Atype == AttackType.Daggers)
                            target.addUniqueLocal("Poison", 1, self);
                        return damage;
                    }));
                    self.addBuff(b);
                });
                case "Piercing Daggers": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff b = new Buff(cardName, -1, null, self);
                    b.AddActivation(new OnAttack( (damage, target, Dtype, Atype) => {
                        if(Atype == AttackType.Daggers)
                            target.addStatBuff("Armor", -1, 1, b.Self);
                        return damage;
                    }));
                    self.addBuff(b);
                });


                
                case "Blinding Poison": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    cardTarget.addUniqueLocal("Poison", 5, self);
                    cardTarget.addUniqueLocal("Blind", 1, self);
                });
                case "Paralyzing Poison": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    cardTarget.addUniqueLocal("Poison", 5, self);
                    cardTarget.addUniqueLocal("Stun", 2, self);
                });
                case "Snakebite Slash": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    if(self.Attack(4, cardTarget, phs) != null)
                        cardTarget.addUniqueLocal("Poison", 2, self);
                    if(self.Attack(4, cardTarget, phs) != null)
                        cardTarget.addUniqueLocal("Poison", 2, self);
                });
                case "Assassin's Technique": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    cardTarget.addUniqueLocal("Poison", 4, self);
                    self.addStatBuff("Dodge", 2, 2, self);
                });
                case "Plague": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    Lobby.loopThroughEnemies(e=>e.addUniqueLocal("Poison", 4, self));
                });


                case "Mocking Shadows": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Taunt(Lobby.getEnemyFromID(cardTarget.Id));
                    self.addStatBuff("Dodge", 2, 1, self);
                });
                case "Bear Trap": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addTrapBuff(cardName, 3, self, (damage, attacker, Dtype, Atype) => attacker.Defend(null, 10, phs, AttackType.Base));
                });
                case "Blinding Trap": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addTrapBuff(cardName, 3, self, (damage, attacker, Dtype, Atype) => attacker.addUniqueLocal("Blind", 1, self));
                });
                case "Poison Trap": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addTrapBuff(cardName, 3, self, (damage, attacker, Dtype, Atype) => attacker.addUniqueLocal("Poison", 4, self));
                });
                case "Heads Up": return new Card(cardName, ally, (self, cardTarget, card) => {
                    cardTarget.addStatBuff("Dodge", 3, 1, self);
                });

                
                case "Infect": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    cardTarget.addUniqueLocal("Poison", card.locals["x"], self);
                    card.locals["x"] = 5;
                }, (self, card) =>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 10);
                    else card.locals["x"] = 10; 
                });
                case "Tempest": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    int hits = card.locals["x"];
                    for(int i = 0; i < hits; i++){
                        if(i == hits - 1){
                            bool a1 = cardTarget.Alive;
                            self.Attack(5, cardTarget, phs);
                            if(a1 && !cardTarget.Alive)
                                card.locals["x"]++;
                        }
                        else self.Attack(5, cardTarget, phs);
                    }
                }, (self, card) =>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 3);
                });
                case "Shadow": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff b = new Buff(cardName, card.locals["x"], typeof(BeforeTurn), self);
                    b.AddActivation(new BeforeTurn(()=>{
                        b.Self.addStatBuff("Dodge", 1, 1, self);
                    }));
                    self.addBuff(b);
                    card.locals["x"]++;
                }, (self, card) =>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 4);
                    else card.locals["x"] = 4; 
                });
                #endregion
                
                #endregion
                #region mage cards

                #region starter
                case "Aether Bolt": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    Buff b = new Buff("", 1, typeof(TurnTaken), self, true);
                    b.AddActivation(new AfterHealthChanged(prev => {
                        int dif = prev - b.Self.Health;
                        if(dif > 0) self.Resource += dif;
                    }));
                    cardTarget.addBuff(b);
                    if(self.ClassAbilityActive) self.AttackI(20, cardTarget, mag);
                    else self.AttackI(10, cardTarget, mag);
                    b.Remove();


                });
                case "Aether Barrier": return new Card(cardName, null, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    Buff buff = Entity.createStatBuff(cardName+(ult ? " (Unleashed)" : ""), "Shield", ult ? 20 : 10, ult?3:2, self);
                    buff.AddActivation(new OnDamage( (damage) => {
                        self.Resource += damage > buff.locals["Shield"] ? buff.locals["Shield"] : damage;
                        return damage;
                    }));
                    buff.AddEffect("Shield");
                    self.addBuff(buff);
                });
                case "Explosion": return new Card(cardName, null, (self, cardTarget, card) => {
                    if(self.ClassAbilityActive) self.AttackAoE(15, mag);
                    else self.AttackAoE(6, mag);
                    
                });
                case "Curse": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    cardTarget.addHexBuff(cardName, 10, 3, self, () =>{
                        cardTarget.addStatBuff("Resolve", ult?-2:-1, ult?4:3, self);
                        cardTarget.addStatBuff("Intellect", ult?-2:-1, ult?4:3, self);
                    });
                });
                case "Arcane Charge": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addUniqueLocal("Burst", (self.ClassAbilityActive ? 15 : 7), self);
                });
                case "Enhance Arcana": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Intellect", (self.ClassAbilityActive ? 5 : 2), -1, self);
                });
                #endregion

                #region beginner
                case "Magic Missiles": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    for(int i = 0; i < (self.ClassAbilityActive ? 5 : 3); i++)
                        self.Attack((self.ClassAbilityActive ? 6 : 5), cardTarget, mag);
                });
                case "Ancient Knowledge": return new Card(cardName, null, (self, cardTarget, card) => {
                    if(self.ClassAbilityActive) self.Resource += 30 + (3 * self.getTotalValue("Intellect"));
                    else self.Resource += 15 + 2 * self.getTotalValue("Intellect");
                });
                case "Strange Brew": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addUniqueLocal("Poison", 3, self);
                    int p = (self.ClassAbilityActive ? 3 : 5);
                    self.addStatBuff("Intellect", p * 2, p, self, true);

                });
                case "Meditate": return new Card(cardName, null, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    Buff b = new Buff(cardName+(self.ClassAbilityActive ? " (Unleashed)" : ""), (ult ? 8 : 6), typeof(BeforeTurn), self);
                    b.AddActivation(new BeforeTurn(()=>{
                        b.Self.addStatBuff("Intellect", 1, (ult ? 6 : 4), b.Self);
                    }));
                    self.addBuff(b);
                });
                case "Arcane Armor": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Shield", (self.ClassAbilityActive?30:10) + (self.getTotalValue("Intellect") * (self.ClassAbilityActive ? 3 : 2)), 2, self);
                });


                case "Death Wish": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    Buff buff = null;
                    buff = cardTarget.addHexBuff(cardName, 10, 3, self, ()=>{
                        buff.Self.loopThrough(b => {if(b.BuffType == BuffClass.Hex) b.locals.canChange = false;});
                        cardTarget.Defend(null, ult?15:5, mag, AttackType.Base);
                        buff.Self.loopThrough(b => {if(b.BuffType == BuffClass.Hex) b.locals.canChange = true;});
                    });
                });
                case "Aether Charm": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    cardTarget.addHexBuff(cardName, 10, 3, self, ()=>{
                        self.Resource += ult?15:5;
                    });
                });
                case "Venom Mark": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    cardTarget.addHexBuff(cardName, 5, 1, self, ()=>{
                        cardTarget.addUniqueLocal("Poison", ult?3:1, null);
                    });
                });
                case "Necrotic Blast": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    self.Attack(ult?50:25, cardTarget, mag);
                    self.Health -= ult?5:10;
                });
                case "Rend": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    int p = self.ClassAbilityActive?5:3;
                    cardTarget.addStatBuff("Armor", p*-1, p, self, true);
                    cardTarget.addStatBuff("Resolve", p*-1, p, self, true);

                    self.addStatBuff("Shield", p*5, p, self, true);
                });


                case "Aether Reaver": return new Card(cardName, null, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    self.AttackAoE(ult?10:5, mag, (dmg, tar)=>{
                        if(dmg != null)
                            self.Resource += ult?5:3;
                    });
                });
                case "Maelstrom": return new Card(cardName, null, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    for(int i = 0; i < (ult?4:2); i++)
                        self.AttackAoE(ult?5:4, mag);
                });
                case "Fracture": return new Card(cardName, null, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    Buff b = new Buff(cardName+(self.ClassAbilityActive ? " (Unleashed)" : ""), -1, null, self);
                    b.AddActivation(new AfterAttack((damage, target, Dtype, Atype)=>{
                        if(Atype == AttackType.Burst){
                            target.addStatBuff("Armor", ult?-4:-2, ult?4:3, self);
                            target.addStatBuff("Resolve", ult?-4:-2, ult?4:3, self);
                        }
                    }));
                    b.AddActivation(new AfterAoE((dmg, hits, Dtype, burst)=>{
                        if(burst)
                            b.Remove();
                    }));
                    self.addBuff(b);
                });
                case "Flash Bomb": return new Card(cardName, null, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    Buff b = new Buff(cardName+(self.ClassAbilityActive ? " (Unleashed)" : ""), -1, null, self);
                    b.AddActivation(new AfterAttack((damage, target, Dtype, Atype)=>{
                        if(Atype == AttackType.Burst){
                            target.addUniqueLocal(ult?"Blind":"Stun", 2, self);
                        }
                    }));
                    b.AddActivation(new AfterAoE((dmg, hits, Dtype, burst)=>{
                        if(burst)
                            b.Remove();
                    }));
                    self.addBuff(b);
                });
                case "Blast Shield": return new Card(cardName, null, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    self.addUniqueLocal("Burst", ult?10:5, self);
                    self.addStatBuff("Shield", ult?15:10, ult?2:1, self);
                });



                case "Devour Intellect": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(10, cardTarget, mag);
                    if(!cardTarget.Alive){
                        self.addStatBuff("Intellect", 4, -1, self);
                        if(self.ClassAbilityActive){
                            self.addStatBuff("Intellect", 1, -1, self);
                            card.locals["x"]++;
                        }
                    }
                }, (self, card)=>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 0);
                    if(card.locals["x"] > 0)
                        self.addStatBuff("Intellect", card.locals["x"], -1, self);
                });
                case "Nova Charge": return new Card(cardName, null, (self, cardTarget, card) => {
                    bool ult = self.ClassAbilityActive;
                    Buff b = new Buff("Nova Charge", card.locals["x"], typeof(BeforeTurn), self);
                    b.AddActivation(new BeforeTurn(()=>{
                        b.Self.addUniqueLocal("Burst", ult?7:3, b.Self);
                    }));
                    self.addBuff(b);
                    card.locals["x"]++;
                }, (self, card)=>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 4);
                    card.locals["x"] = 4;
                });
                case "Scourge": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    int d = card.locals["x"];
                    cardTarget.addStatBuff("Armor", d * -1, d, self, !self.ClassAbilityActive);
                    cardTarget.addStatBuff("Resolve", d * -1, d, self, !self.ClassAbilityActive);
                    if(card.locals["x"] > 3) card.locals["x"]--;
                }, (self, card)=>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 6);
                    card.locals["x"] = 6;
                });
                #endregion

                #endregion
                #region cleric cards

                #region starter
                case "Holy Strike": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(7, cardTarget, phs);
                    cardTarget.addStatBuff("Resolve", -1, 1, self);
                    cardTarget.addStatBuff("Armor", -1, 1, self);
                });
                case "Guard": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addStatBuff("Shield", 15, 3, self, true);
                });
                case "Restore": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.Heal(10, self);
                });
                case "Divinity": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addUniqueLocal("Smite", 4, self);
                });
                case "Inspire": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addStatBuff("Strength", 3, 3, self, true);
                    cardTarget.addStatBuff("Intellect", 3, 3, self, true);
                });
                case "Enfeeble": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(10, cardTarget, mag);
                    cardTarget.addStatBuff("Strength", -2, 2, self);
                    cardTarget.addStatBuff("Intellect", -2, 2, self);
                });
                #endregion

                #region beginner
                case "Urgent Defense": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addStatBuff("Shield", (int)(30f - (20f * (float)cardTarget.Health / (float)cardTarget.MaxHealth)), 1, self);
                });
                case "Sip of Ichor": return new Card(cardName, player, (self, cardTarget, card) => {
                    int dur = 3;
                    int stat = 3;
                    cardTarget.addStatBuff("Armor", stat, dur, self);
                    cardTarget.addStatBuff("Resolve", stat, dur, self);
                    cardTarget.addStatBuff("Strength", stat, dur, self);
                    cardTarget.addStatBuff("Intellect", stat, dur, self);
                });
                case "Revitalize": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addStatBuff("Regen", 3, 5, self);
                });
                case "Healing Prayer": return new Card(cardName, null, (self, cardTarget, card) => {
                    foreach(Player p in Lobby.players.Values)
                        p.Heal(7, self);
                });
                case "Exalt": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addStatBuff("Strength", 2, -1, self);
                    cardTarget.addStatBuff("Intellect", 2, -1, self);
                });


                case "Regenerative Plating": return new Card(cardName, player, (self, cardTarget, card) => {
                    Buff buff = new Buff(cardName, 6, typeof(BeforeTurn), self);
                    buff.AddActivation(new BeforeTurn( () => {
                        buff.Self.addStatBuff("Shield", 5, 1, self);
                    }));
                    cardTarget.addBuff(buff);
                });
                case "Spiked Shield": return new Card(cardName, player, (self, cardTarget, card) => {
                    Buff buff = new Buff(cardName, -1, null, self);
                    buff.locals.Add("shielded", self.getTotalValue("Shield"));
                    buff.AddActivation(new TurnTaken( () => {
                        buff.locals["shielded"] = buff.Self.getTotalValue("Shield");
                    }));
                    buff.AddActivation(new AfterTryDefend( (damage, attacker, Dtype, Atype) => {
                        if(buff.locals["shielded"] > 0 && attacker != null) attacker.takeDamage(5);
                    }));
                    cardTarget.addBuff(buff);
                });
                case "Divine Shield": return new Card(cardName, player, (self, cardTarget, card) => {
                    Buff buff = new Buff(cardName, -1, null, self);
                    buff.locals.Add("Regen", 0);
                    buff.AddEffect("Regen");
                    buff.AddActivation(new TurnTaken( () => {
                        if(buff.Self.getTotalValue("Shield") > 0) buff.locals["Regen"] = 2;
                        else buff.locals["Regen"] = 0;
                    }));
                    cardTarget.addBuff(buff);
                });
                case "Enchanted Shield": return new Card(cardName, player, (self, cardTarget, card) => {
                    Buff buff = new Buff(cardName, -1, null, self);
                    buff.locals.Add("Armor", 0);
                    buff.locals.Add("Resolve", 0);
                    buff.AddEffect("Armor");
                    buff.AddEffect("Resolve");
                    buff.AddActivation(new TurnTaken( () => {
                        if(buff.Self.getTotalValue("Shield") > 0) {buff.locals["Armor"] = 2;buff.locals["Resolve"] = 2;}
                        else {buff.locals["Armor"] = 0;buff.locals["Resolve"] = 0;}
                    }));
                    cardTarget.addBuff(buff);
                });
                case "Perseverance": return new Card(cardName, null, (self, cardTarget, card) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Shield", 10, 2, self, true);
                    }
                });


                case "Radiance": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff buff = new Buff(cardName, -1, null, self);
                    buff.AddActivation(new OnAttack((damage, target, Dtype, Atype) => {
                        if(Atype == AttackType.Smite)
                            damage+=3;
                        return damage;
                    }));
                    self.addBuff(buff);
                });
                case "Judgement": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff buff = new Buff(cardName, -1, null, self);
                    buff.AddActivation(new AfterAttack((damage, target, Dtype, Atype) => {
                        if(Atype == AttackType.Smite){
                            target.addStatBuff("Armor", -2, 2, self);
                            target.addStatBuff("Resolve", -2, 2, self);
                        }
                    }));
                    self.addBuff(buff);
                });
                case "Divine Protection": return new Card(cardName, null, (self, cardTarget, card) => {
                    self.addStatBuff("Shield", 10, 2, self);
                    self.addUniqueLocal("Smite", 3, self);
                });
                case "Honor": return new Card(cardName, null, (self, cardTarget, card) => {
                    Buff buff = new Buff(cardName, -1, null, self);
                    buff.AddActivation(new AfterAttack((damage, target, Dtype, Atype) => {
                        if(Atype == AttackType.Smite)
                            self.addStatBuff("Regen", 1, 5, self);
                    }));
                    self.addBuff(buff);
                });
                case "Twinsmite Strike": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    self.Attack(10, cardTarget, phs);
                    Buff b = self.getUniqueBuff("Smite");
                    if(b != null)
                        b.GetActivation<AfterAttack>().D(0, cardTarget, phs, AttackType.Base);
                });


                case "Redemption": return new Card(cardName, ally, (self, cardTarget, card) => {
                    int h = cardTarget.Health;
                    cardTarget.Heal(card.locals["x"], self);
                    if(h <= 15) card.locals["x"] += 3;
                }, (self, card)=>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 15);
                });
                case "Safeguard": return new Card(cardName, player, (self, cardTarget, card) => {
                    cardTarget.addStatBuff("Shield", card.locals["x"], -1, self);

                    card.locals["x"] -= 5;

                    if(card.locals["x"] < 5) card.locals["x"] = 5;
                }, (self, card)=>{
                    if(!card.locals.ContainsKey("x"))
                        card.locals.Add("x", 20);
                    card.locals["x"] = 20;
                });
                case "Regicide": return new Card(cardName, enemy, (self, cardTarget, card) => {
                    int attacks = Lobby.enemies.Count == 1 ? 3 : 2;
                    for(int i = 0; i < attacks; i++)
                        self.Attack(7, cardTarget, phs);
                });
                #endregion
                
                #endregion
                            
                case "": return null;
                default: throw new Exception("Card "+cardName+" does not exist");
            }
        }
    }
}