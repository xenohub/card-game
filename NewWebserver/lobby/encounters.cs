using System.Collections.Generic;
using System.Collections;
using System;
using classes;

namespace classes{
    public partial class Enemy {
        const DamageType phs = DamageType.Physical;
        const DamageType mag = DamageType.Magic;
        static int random(double[] probDist){
            double r = rand.NextDouble();
            int i = 0;
            for(;i < probDist.Length; i++){
                r -= probDist[i];
                if(r < 0) break;
            }
            return i;
        }
        /*static int scaling(int start, double scaling){ //old scaling 
            int var = start + (int)(((double)(Lobby.players.Count - 2)) * scaling);
            return var;
        }*/
        static int getEDV(){ //new scaling
            return (int)Math.Ceiling(((float)Lobby.players.Count) / 2f);
        }
        
        public static Enemy[] getEncounter(int floorNum){
            switch(floorNum){
                case 1:
                switch(random(new double[]{.7,.3})){
                    case 0:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead"});
                    default:
                    return getEnemyArray(new string[]{"Basic Undead", "Zombie"});
                }
                case 2:
                switch(random(new double[]{.3,.4,.3})){
                    case 0:
                    return getEnemyArray(new string[]{"Small Slime", "Small Slime", "Small Slime", "Small Slime", "Small Slime", "Small Slime"});
                    case 1:
                    return getEnemyArray(new string[]{"Small Slime", "Small Slime", "Small Slime", "Slime", });
                    default:
                    return getEnemyArray(new string[]{"Psychic Slime", "Acid Slime"});
                }
                case 3:
                switch(random(new double[]{.7, .3})){
                    case 0:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead", "Zombie"});
                    default:
                    return getEnemyArray(new string[]{"Basic Undead", "Zombie", "Zombie"});
                }
                case 4:
                switch(random(new double[]{.4, .4, .2})){
                    case 0:
                    return getEnemyArray(new string[]{"Acid Slime", "Skeleton", "Skeleton"});
                    case 1:
                    return getEnemyArray(new string[]{"Psychic Slime", "Spectre", "Spectre"});
                    default:
                    return getEnemyArray(new string[]{"Small Slime", "Small Slime", "Small Slime", "Spectre", "Skeleton"});
                }
                case 5:
                switch(random(new double[]{.3, .3, .4})){
                    case 0:
                    return getEnemyArray(new string[]{"Wraith"});
                    case 1:
                    return getEnemyArray(new string[]{"Revenant"});
                    default:
                    return getEnemyArray(new string[]{"Large Slime"});
                }
                case 6:
                switch(random(new double[]{.30, .40, .30})){
                    case 0:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead", "Basic Undead", "Witch"});
                    case 1:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead", "Zombie", "Witch"});
                    default:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead", "Witch", "Witch"});
                }
                case 7:
                switch(random(new double[]{.2, .3, .2, .3})){
                    case 0:
                    return getEnemyArray(new string[]{"Acid Slime", "Acid Slime", "Zombie"});
                    case 1:
                    return getEnemyArray(new string[]{"Acid Slime", "Skeleton", "Zombie"});
                    case 2:
                    return getEnemyArray(new string[]{"Psychic Slime", "Psychic Slime", "Witch"});
                    default:
                    return getEnemyArray(new string[]{"Psychic Slime", "Spectre", "Witch"});
                }
                case 8:
                switch(random(new double[]{.6, .4})){
                    case 1:
                    return getEnemyArray(new string[]{"Acid Slime", "Psychic Slime", "Slime", "Slime"});
                    default:
                    return getEnemyArray(new string[]{"Acid Slime", "Psychic Slime", "Slime", "Small Slime", "Small Slime"});
                }
                case 9:
                switch(random(new double[]{.3, .4, .3})){
                    case 0:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead", "Basic Undead", "Advanced Undead", "Advanced Undead"});
                    case 1:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead", "Advanced Undead", "Advanced Undead", "Advanced Undead"});
                    default:
                    return getEnemyArray(new string[]{"Basic Undead", "Witch", "Zombie", "Advanced Undead", "Advanced Undead"});
                }
                case 10:
                switch(random(new double[]{.5, .5})){
                    case 0:
                    return getEnemyArray(new string[]{"Necromaster"});
                    default:
                    return getEnemyArray(new string[]{"Mother Slime"});
                }
                default:
                switch(random(new double[]{.2, .2, .1, .2, .2, .1})){
                    case 0:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead", "Basic Undead", "Basic Undead", "Basic Undead", "Basic Undead", "Basic Undead", "Basic Undead"});
                    case 1:
                    return getEnemyArray(new string[]{"Basic Undead", "Basic Undead", "Basic Undead", "Basic Undead", "Advanced Undead", "Advanced Undead", "Advanced Undead", "Advanced Undead"});
                    case 2:
                    return getEnemyArray(new string[]{"Zombie", "Zombie", "Zombie", "Zombie", "Zombie", "Zombie", "Zombie", "Zombie"});
                    case 3:
                    return getEnemyArray(new string[]{"Large Slime", "Large Slime"});
                    case 4:
                    return getEnemyArray(new string[]{"Slime", "Slime", "Slime", "Slime"});
                    default:
                    return getEnemyArray(new string[]{"Small Slime", "Small Slime", "Small Slime", "Small Slime", "Small Slime", "Small Slime", "Small Slime", "Small Slime"});
                }
            }
            throw new Exception();
        }

        public static Enemy[] getEnemyArray(string[] arr){
            Enemy[] enemy = new Enemy[arr.Length]; 
            for(int i = 0; i < arr.Length; i++) enemy[i] = GetEnemy(arr[i]);
            return enemy;
        }

        public static bool EnemyExists(string enemyName){
            try{
                GetEnemy(enemyName);
                return true;
            }
            catch{
                return false;
            }
        }

        public static Enemy GetEnemy(string enemyName){ //debuff, buff, utility 
            int playerNumber = Lobby.players.Count;
            int floorNum = Lobby.encounterNumber;
            Player[] players = new Player[Lobby.players.Count]; //also if the enemy is doing an aoe attack to all players, leave the targeting array blank
            Lobby.players.Values.CopyTo(players, 0);
            switch(enemyName){
                #region shorthand
                case "Small Slime": if(rand.Next(2) == 0) return GetEnemy("Small Acid Slime"); else return GetEnemy("Small Psychic Slime");
                case "Slime": if(rand.Next(2) == 0) return GetEnemy("Acid Slime"); else return GetEnemy("Psychic Slime");
                case "Large Slime": if(rand.Next(2) == 0) return GetEnemy("Large Acid Slime"); else return GetEnemy("Large Psychic Slime");
                case "Undead": if(rand.Next(2) == 0) return GetEnemy("Basic Undead"); else return GetEnemy("Advanced Undead");
                case "Basic Undead": if(rand.Next(2) == 0) return GetEnemy("Skeleton"); else return GetEnemy("Spectre");
                case "Advanced Undead": if(rand.Next(2) == 0) return GetEnemy("Zombie"); else return GetEnemy("Witch");
                #endregion

                #region Undead
                case "Skeleton": return new Enemy(enemyName, 50, (self) => {
                    switch(random(new double[]{.4, .3, .3})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        default: throw new Exception();
                    }
                }, self=>{}, self=>false, new Intention[]{ 
                new Intention(5, phs, getEDV(), 1, new bool[]{false, false, false}, (self, target) => {
                    self.Attack(5, target, phs);
                }), 
                new Intention(4, phs, getEDV(), 2, new bool[]{false, false, false}, (self, target) => {
                    self.Attack(4, target, phs);
                    self.Attack(4, target, phs);
                }),
                new Intention(5, phs, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                    if(self.Attack(5, target, phs)!= null)
                        target.addStatBuff("Armor", -2, 2, self);
                })});

                case "Spectre": return new Enemy(enemyName, 50, (self) => {
                    switch(random(new double[]{.4, .3, .3})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        default: throw new Exception();
                    }
                }, self=>{}, self=>false, new Intention[]{ 
                new Intention(5, mag, getEDV(), 1, new bool[]{false, false, false}, (self, target) => {
                    self.Attack(5, target, mag);
                }), 
                new Intention(4, mag, -1, 1, new bool[]{false, false, false}, (self, target) => {
                    self.AttackAoE(4, mag);
                }),
                new Intention(5, mag, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                    if(self.Attack(5, target, mag)!= null)
                        target.addStatBuff("Resolve", -2, 2, self);
                })});

                case "Zombie": return new Enemy(enemyName, 50, (self) => {
                    switch(random(new double[]{.4, .4, .2})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        default: throw new Exception();
                    }
                }, (self) => {
                    Buff b = new Buff("Undead Hunger", -1, null, self);
                    b.AddActivation(new AfterAttack((damage, target, Dtype, Atype)=>{
                        self.Health += damage / 2;
                    }));
                    self.addBuff(b);
                }, self=>false, new Intention[]{ 
                new Intention(5, phs, getEDV(), 1, new bool[]{false, false, false}, (self, target) => {
                    self.Attack(5, target, phs);
                }), 
                new Intention(5, phs, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(5, target, phs)!= null)
                            target.addStatBuff("Strength", -2, 2, self);
                }), 
                new Intention(0, null, 0, 0, new bool[]{false, true, false}, (self, target) => {
                    self.addStatBuff("Regen", 1+getEDV(), -1, self);
                })});

                case "Witch": return new Enemy(enemyName, 50, (self) => {
                    double[] prob = new double[]{.2, .4, .4};
                    if(Lobby.enemies.Find(e=>e.name != "Witch") == null) prob = new double[]{1};
                    switch(random(prob)){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        default: throw new Exception();
                    }
                }, self=>{}, self=>{
                    if(Lobby.enemies.Find(e=>e.name != "Witch") == null && 
                        (self.PlannedAction == self.behaviors[1] || self.PlannedAction == self.behaviors[2])) 
                        return true;
                    else return false;
                }, new Intention[]{ 
                new Intention(3, mag, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(3, target, mag) != null)
                            target.addStatBuff("Intellect", -2, 2, self);
                }),
                new Intention(0, null, 0, 0, new bool[]{false, true, false}, (self, target) => {
                    Enemy e;
                    do e = Lobby.enemies[rand.Next(Lobby.enemies.Count)]; while(e.name == self.name);
                    e.addStatBuff("Shield", 5*getEDV(), 1, self);
                    e.addStatBuff("Armor", 2, 1, self);
                    e.addStatBuff("Resolve", 2, 1, self);
                }), 
                new Intention(0, null, 0, 0, new bool[]{false, true, false}, (self, target) => {
                    Enemy e;
                    do e = Lobby.enemies[rand.Next(Lobby.enemies.Count)]; while(e.name == self.name);
                    e.Heal(5*getEDV(), self);
                    e.addStatBuff("Strength", 2, -1, self);
                    e.addStatBuff("Intellect", 2, -1, self);
                })});
                #endregion

                #region Slimes
                case "Small Acid Slime": return new Enemy(enemyName, 15, (self) => {
                    switch(random(new double[]{.4, .3, .3})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        default: throw new Exception();
                    }
                }, (self) => {}, self=>false, new Intention[]{ 
                new Intention(2, phs, getEDV(), 1, new bool[]{false, false, false}, (self, target) => {
                    self.Attack(2, target, phs);
                }),
                new Intention(2, phs, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(2, target, phs) != null)
                            target.addStatBuff("Strength", -1, 1, self, true);
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Strength", -1, 1, self, true);
                        p.addStatBuff("Armor", -1, 1, self, true);
                    }
                })});

                case "Acid Slime": return new Enemy(enemyName, 60, (self) => {
                    switch(random(new double[]{.4, .3, .3})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        default: throw new Exception();
                    }
                }, (self) => {
                    Buff b = new Buff("Split", -1, null, self);
                    b.locals.Add("x", self.Health / 2);
                    b.AddActivation(new EndRound(()=>{
                        b.locals["x"] = b.Self.Health / 2;
                    }));
                    b.AddActivation(new AfterDamage((damage)=>{
                        b.locals["x"] = b.locals["x"] - damage < 0 ? 0 : b.locals["x"] - damage;
                        if(b.locals["x"] == 0)
                            self.PlannedAction = self.behaviors[3];
                    }));
                    self.addBuff(b);
                }, self=>false, new Intention[]{ 
                new Intention(4, phs, getEDV(), 1, new bool[]{false, false, false}, (self, target) => {
                    self.Attack(4, target, phs);
                }),
                new Intention(4, phs, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(4, target, phs) != null)
                            target.addStatBuff("Strength", -2, 2, self, true);
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Strength", -2, 2, self, true);
                        p.addStatBuff("Armor", -2, 2, self, true);
                    }
                }),
                new Intention(0, null, 0, 0, new bool[]{false, false, true}, (self, target) => {
                    float hp = self.Health;
                    Enemy e1 = Lobby.spawnEnemy(Enemy.GetEnemy("Small Acid Slime"));
                    e1.Health = (int)Math.Ceiling(hp / 2);
                    self.die();
                    Enemy e2 = Lobby.spawnEnemy(Enemy.GetEnemy("Small Acid Slime"));
                    e2.Health = (int)Math.Ceiling(hp / 2);
                })});
                
                case "Small Psychic Slime": return new Enemy(enemyName, 15, (self) => {
                    switch(random(new double[]{.4, .3, .3})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        default: throw new Exception();
                    }
                }, (self) => {}, self=>false, new Intention[]{ 
                new Intention(2, mag, getEDV(), 1, new bool[]{false, false, false}, (self, target) => {
                    self.Attack(2, target, mag);
                }),
                new Intention(2, mag, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(2, target, mag) != null)
                            target.addStatBuff("Intellect", -1, 1, self, true);
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Intellect", -1, 1, self, true);
                        p.addStatBuff("Resolve", -1, 1, self, true);
                    }
                })});

                case "Psychic Slime": return new Enemy(enemyName, 60, (self) => {
                    switch(random(new double[]{.4, .3, .3})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        default: throw new Exception();
                    }
                }, (self) => {
                    Buff b = new Buff("Split", -1, null, self);
                    b.locals.Add("x", self.Health / 2);
                    b.AddActivation(new EndRound(()=>{
                        b.locals["x"] = b.Self.Health / 2;
                    }));
                    b.AddActivation(new AfterDamage((damage)=>{
                        b.locals["x"] = b.locals["x"] - damage < 0 ? 0 : b.locals["x"] - damage;
                        if(b.locals["x"] == 0)
                            self.PlannedAction = self.behaviors[3];
                    }));
                    self.addBuff(b);
                }, self=>false, new Intention[]{ 
                new Intention(4, mag, getEDV(), 1, new bool[]{false, false, false}, (self, target) => {
                    self.Attack(4, target, mag);
                }),
                new Intention(4, mag, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(4, target, mag) != null)
                            target.addStatBuff("Intellect", -2, 2, self, true);
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Intellect", -2, 2, self, true);
                        p.addStatBuff("Resolve", -2, 2, self, true);
                    }
                }),
                new Intention(0, null, 0, 0, new bool[]{false, false, true}, (self, target) => {
                    float hp = self.Health;
                    Enemy e1 = Lobby.spawnEnemy(Enemy.GetEnemy("Small Psychic Slime"));
                    e1.Health = (int)Math.Ceiling(hp / 2);
                    self.die();
                    Enemy e2 = Lobby.spawnEnemy(Enemy.GetEnemy("Small Psychic Slime"));
                    e2.Health = (int)Math.Ceiling(hp / 2);
                })});
                #endregion

                #region Minibosses
                case "Large Acid Slime": return new Enemy(enemyName, 240, (self) => {
                    switch(random(new double[]{.6, .4})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        default: throw new Exception();
                    }
                }, (self) => {
                    Buff b = new Buff("Volatile Split", -1, null, self);
                    b.locals.Add("x", self.Health / 5);
                    b.AddActivation(new EndRound(()=>{
                        b.locals["x"] = b.Self.Health / 5;
                    }));
                    b.AddActivation(new AfterDamage((damage)=>{
                        b.locals["x"] = b.locals["x"] - damage < 0 ? 0 : b.locals["x"] - damage;
                        if(b.locals["x"] == 0)
                            self.PlannedAction = self.behaviors[2];
                    }));
                    self.addBuff(b);
                }, self=>false, new Intention[]{ 
                new Intention(8, phs, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(8, target, mag) != null)
                            target.addStatBuff("Strength", -3, 3, self, true);
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Strength", -3, 3, self, true);
                        p.addStatBuff("Armor", -3, 3, self, true);
                    }
                }),
                new Intention(0, null, 0, 0, new bool[]{false, false, true}, (self, target) => {
                    float hp = self.Health;
                    Enemy e1 = Lobby.spawnEnemy(Enemy.GetEnemy("Acid Slime"));
                    e1.Health = (int)Math.Ceiling(hp / 2);
                    self.die();
                    Enemy e2 = Lobby.spawnEnemy(Enemy.GetEnemy("Acid Slime"));
                    e2.Health = (int)Math.Ceiling(hp / 2);
                })});
                
                case "Large Psychic Slime": return new Enemy(enemyName, 240, (self) => {
                    switch(random(new double[]{.6, .4})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        default: throw new Exception();
                    }
                }, (self) => {
                    Buff b = new Buff("Volatile Split", -1, null, self);
                    b.locals.Add("x", self.Health / 5);
                    b.AddActivation(new EndRound(()=>{
                        b.locals["x"] = b.Self.Health / 5;
                    }));
                    b.AddActivation(new AfterDamage((damage)=>{
                        b.locals["x"] = b.locals["x"] - damage < 0 ? 0 : b.locals["x"] - damage;
                        if(b.locals["x"] == 0)
                            self.PlannedAction = self.behaviors[2];
                    }));
                    self.addBuff(b);
                }, self=>false, new Intention[]{ 
                new Intention(8, mag, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(8, target, mag) != null)
                            target.addStatBuff("Intellect", -3, 3, self, true);
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Intellect", -3, 3, self, true);
                        p.addStatBuff("Resolve", -3, 3, self, true);
                    }
                }),
                new Intention(0, null, 0, 0, new bool[]{false, false, true}, (self, target) => {
                    float hp = self.Health;
                    Enemy e1 = Lobby.spawnEnemy(Enemy.GetEnemy("Psychic Slime"));
                    e1.Health = (int)Math.Ceiling(hp / 2);
                    self.die();
                    Enemy e2 = Lobby.spawnEnemy(Enemy.GetEnemy("Psychic Slime"));
                    e2.Health = (int)Math.Ceiling(hp / 2);
                })});
                
                case "Revenant": return new Enemy(enemyName, 200, (self) => {
                    if(self.Locals["buff"] == 0) return self.behaviors[2];
                    if(rand.Next(2) == 1) return self.behaviors[0];
                    else return self.behaviors[1];
                }, (self) => {
                    self.Locals.Add("buff", 0);
                }, self=>false, new Intention[]{ 
                new Intention(4, phs, getEDV(), 3, new bool[]{false, false, false}, (self, target) => {
                        self.Attack(4, target, phs);
                        self.Attack(4, target, phs);
                        self.Attack(4, target, phs);
                }),
                new Intention(8, phs, 1, getEDV(), new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(8, target, phs) != null)
                            target.addStatBuff("Armor", -3, 3, self);
                }), 
                new Intention(0, null, 0, 0, new bool[]{false, false, true}, (self, target)=>{
                    Buff b = new Buff("Undead Rage", -1, null, self);
                    b.locals.Add("Strength", 0);
                    b.locals.Add("added", 0);
                    b.AddActivation(new AfterDefend( (damage, attacker, Dtype, Atype) => {
                        if((Atype == AttackType.Base || Atype == AttackType.AoE) && b.locals["added"] < 4) {
                            b.locals["Strength"]++;
                            b.locals["added"]++;
                        }
                    }));
                    b.AddActivation(new BeforeTurn(()=>{
                        b.locals["added"] = 0;
                    }));
                    b.AddEffect("Strength");
                    self.addBuff(b);
                    self.Locals["buff"] = 1;
                })});

                case "Wraith": return new Enemy(enemyName, 200, (self) => {
                    if(self.Locals["buff"] == 0) return self.behaviors[2];
                    if(rand.Next(2) == 1) return self.behaviors[0];
                    else return self.behaviors[1];
                }, (self) => {
                    self.Locals.Add("buff", 0);
                }, self=>false, new Intention[]{ 
                new Intention(6, mag, -1, 1, new bool[]{false, false, false}, (self, target) => {
                    self.AttackAoE(6, mag);
                }),
                new Intention(8, mag, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(8, target, mag) != null)
                            target.addStatBuff("Resolve", -3, 3, self);
                }), 
                new Intention(0, null, 0, 0, new bool[]{false, false, true}, (self, target)=>{
                    Buff b = new Buff("Undead Wisdom", -1, null, self);
                    b.locals.Add("Intellect", 0);
                    b.AddActivation(new AfterTurn( () => {
                        b.locals["Intellect"] += 2;
                    }));
                    b.AddEffect("Intellect");
                    self.addBuff(b);
                    self.Locals["buff"] = 1;
                })});
                #endregion

                #region Bosses
                case "Necromaster": return new Enemy(enemyName, 400, (self) => {
                    double chance = (double)( 4 - (Lobby.enemies.Count - 1) ) / 4d;
                    if(chance > rand.NextDouble()) return self.behaviors[0];
                    switch(random(new double[]{.4, .4, .2})){
                        case 0: return self.behaviors[1];
                        case 1: return self.behaviors[2];
                        case 2: return self.behaviors[3];
                        default: throw new Exception();
                    }
                }, (self) => {
                    self.Locals.Add("spawnedMax", 0);
                    Buff b = new Buff("Necromaster", -1, null, self, true);
                    b.AddActivation(new AfterHealthChanged(prev=>{
                        if(b.Self.Health == 0)
                            Lobby.loopThroughEnemies(e=>{
                                if(e != b.Self) e.die();
                            });
                    }));
                    self.addBuff(b);
                }, self=>false, new Intention[]{ 
                new Intention(0, null, 0, 0, new bool[]{false, false, true}, (self, target) => {
                    int missing = 5 - Lobby.enemies.Count;
                    int spawn = 1;
                    if(spawn > missing) spawn = missing;
                    if(self.Locals["spawnedMax"] == 0) {
                        spawn = missing;
                        self.Locals["spawnedMax"] = 1;
                    }

                    for(int i = 0; i < spawn; i++){
                        switch(random(new double[]{.4, .4, .2})){
                            case 0: Lobby.spawnEnemy(GetEnemy("Skeleton")).MaxHealth /= 2; break;
                            case 1: Lobby.spawnEnemy(GetEnemy("Spectre")).MaxHealth /= 2; break;
                            case 2: Lobby.spawnEnemy(GetEnemy("Zombie")).MaxHealth /= 2; break;
                        }
                    }
                    Lobby.updateUI();
                }),
                new Intention(0, null, -1, 0, new bool[]{false, true, false}, (self, target) => {
                    foreach(Enemy e in Lobby.enemies){
                        if(e.Id != self.Id && (e.name == "Skeleton" || e.name == "Spectre" || e.name == "Zombie")){
                            e.Heal(5*getEDV(), self);
                            e.addStatBuff("Strength", 2, -1, self);
                            e.addStatBuff("Intellect", 2, -1, self);
                        }
                    }
                }),
                new Intention(0, null, getEDV(), 0, new bool[]{true, false, false}, (self, target) => {
                        target.addStatBuff("Armor", -4, 4, self);
                        target.addStatBuff("Resolve", -4, 4, self);
                }), 
                new Intention(0, null, 1, 1, new bool[]{true, false, false}, (self, target) => {
                    Buff b = new Buff("Death Mark", 1, typeof(EndRound), self);
                    b.AddActivation(new StartRound( () => {
                        Player p = Lobby.getPlayerFromID(b.Self.Id);
                        p.addStatBuff("Armor", -5, 1, self);
                        p.addStatBuff("Resolve", -5, 1, self);
                        foreach(Enemy e in Lobby.enemies) 
                            if(e.name == "Skeleton" || e.name == "Spectre" || e.name == "Zombie"){
                                if(e.PlannedAction.Targets < 1){ //note: game freezes if no intention has more than 0 targets
                                    do e.PlannedAction = e.Behaviors[rand.Next(e.Behaviors.Length)]; while(e.PlannedAction.Targets < 1);
                                }
                                e.PlannedAction.targets = new string[]{p.Id};
                                Lobby.enemyIntentionsChanged(e);
                            }
                    }));
                    target.addBuff(b);
                })});
                
                case "Mother Slime": return new Enemy(enemyName, 960, (self) => {
                    switch(random(new double[]{.2, .2, .2, .2, .2})){
                        case 0: return self.behaviors[0];
                        case 1: return self.behaviors[1];
                        case 2: return self.behaviors[2];
                        case 3: return self.behaviors[3];
                        case 4: return self.behaviors[4];
                        default: throw new Exception();
                    }
                }, (self) => {
                    Buff b = new Buff("Explosive Split", -1, null, self);
                    b.locals.Add("x", self.Health / 5);
                    b.AddActivation(new EndRound(()=>{
                        b.locals["x"] = b.Self.Health / 10;
                    }));
                    b.AddActivation(new AfterDamage((damage)=>{
                        b.locals["x"] = b.locals["x"] - damage < 0 ? 0 : b.locals["x"] - damage;
                        if(b.locals["x"] == 0)
                            self.PlannedAction = self.behaviors[4];
                    }));
                    self.addBuff(b);
                }, self=>false, new Intention[]{ 
                new Intention(16, phs, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(16, target, phs) != null)
                            target.addStatBuff("Strength", -4, 4, self, true);
                }),
                new Intention(16, mag, getEDV(), 1, new bool[]{true, false, false}, (self, target) => {
                        if(self.Attack(16, target, mag) != null)
                            target.addStatBuff("Intellect", -4, 4, self, true);
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Intellect", -4, 4, self, true);
                        p.addStatBuff("Resolve", -4, 4, self, true);
                    }
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Strength", -4, 4, self, true);
                        p.addStatBuff("Armor", -4, 4, self, true);
                    }
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, false}, (self, target) => {
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Armor", -1, -1, self);
                        p.addStatBuff("Resolve", -1, -1, self);
                    }
                }),
                new Intention(0, null, -1, 0, new bool[]{true, false, true}, (self, target) => {
                    float hp = self.Health;
                    Enemy e1 = Lobby.spawnEnemy(Enemy.GetEnemy("Large Acid Slime"));
                    e1.Health = (int)Math.Ceiling(hp / 2);
                    self.die();
                    Enemy e2 = Lobby.spawnEnemy(Enemy.GetEnemy("Large Psychic Slime"));
                    e2.Health = (int)Math.Ceiling(hp / 2);
                    foreach(Player p in Lobby.players.Values){
                        p.addStatBuff("Strength", -4, 4, self, true);
                        p.addStatBuff("Intellect", -4, 4, self, true);
                        p.addStatBuff("Armor", -4, 4, self, true);
                        p.addStatBuff("Resolve", -4, 4, self, true);

                    }
                })});
                #endregion
                
                default: throw new Exception("Enemy "+enemyName+" does not exist");
            }
        }
    }
}