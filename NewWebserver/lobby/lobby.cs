using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using classes;
using System;

static class Lobby
{
    #region global methods
    public static void cmd(string msg) {Console.Write("!~"+msg+";");}
    public static void cmd(string user, string msg) {system("to:" + user + ":" + msg);}
    public static void system(string msg) {Console.Write("!~~"+msg+";");}
    public static void log(string msg) {Console.Write(msg+";");}
    public static void sendBackdoor(string user, string msg) {Console.Write("!"+user+" "+msg+";");}
    static Queue<string> messages = new Queue<string>();
    public static void Msg(string msg) {
        cmd("msg:"+encode(msg));
        messages.Enqueue(encode(msg));
        if(messages.Count > 100)
            messages.Dequeue();
    }
   public static string decode (string input) {return System.Net.WebUtility.UrlDecode(input);}
    public static string encode (string input) {return System.Net.WebUtility.UrlEncode(input);}
    #endregion

    static void Main(string[] args){
        run();
        var input = Console.OpenStandardInput();
        var buffer = new byte[1024];
        int length;
        while (input.CanRead && (length = input.Read(buffer, 0, buffer.Length)) > 0 && runThread)
        {
            var payload = new byte[length];
            Buffer.BlockCopy(buffer, 0, payload, 0, length);
            string[] data = System.Text.Encoding.UTF8.GetString(payload).Split(';');
            try {
                for(int i = 0; i < data.Length; i++) EventTriggered(data[i]);
            }
            catch(EncounterEndException e){
               endEncounter(e.Win);
            }
            /*catch(Exception e){
                log("Error");
                //debug();
                System.Runtime.ExceptionServices.ExceptionDispatchInfo.Capture(e).Throw();
            }*/
        }
    }

    #region global vars
    static bool runThread = true;
    static Random rand = new Random();
    public static List<string> playerOrder = new List<string>();
    public static Dictionary<string, Player> players = new Dictionary<string, Player>();
    static public bool Open = true;
    #endregion
    #region preinitialization vars
    static Dictionary<Type, List<string>> classArray = new Dictionary<Type,  List<string>>() {{typeof(Warrior),new List<string>()},{typeof(Rogue),new List<string>()},{typeof(Mage),new List<string>()},{typeof(Cleric),new List<string>()},{typeof(Player),new List<string>()}};
    static Dictionary<string, bool> playersReady = new Dictionary<string, bool>();
    static Dictionary<string, PlayerStatus> playerStatus = new Dictionary<string, PlayerStatus>();
    static Type getClass(string name){
        foreach(Type k in classArray.Keys)
            foreach(string p in classArray[k])
                if(p == name)
                    return k;
        throw new KeyNotFoundException("Key "+name+" not found in classArray");
    }
    #endregion
    #region postinitialization vars
    public static List<Enemy> enemies = new List<Enemy>();
    public static int encounterNumber = 1;
    public static int encounterJump = -1;
    public static int encounterSkip = 0;
    static int turnIndex = 0;
    static Dictionary<string, LobbyCommand> commands = new Dictionary<string, LobbyCommand>();
    static bool canKick = false;
    static Dictionary<string, bool> kicks = new Dictionary<string, bool>();
    #endregion

    static void EventTriggered(string evt){
        if(evt == "") return;
        string[] temparr = evt.Split(':');
        string[] arr = new string[10 + temparr.Length];
        Array.Copy(temparr, arr, temparr.Length);
        evt.Split(':');
        if(inEncounter && arr[0] != "system" && playerOrder[turnIndex] == arr[0]) startAfk();
        #region system commands
        if(arr[0] == "system"){
            if(Open){
                switch(arr[1]){
                    case "backdoor":
                    sendBackdoor(arr[2], "The lobby has not yet been initalized, no commands are availible");
                    break;
                    case "userjoined":
                    playerOrder.Add(arr[2]);
                    playersReady.Add(arr[2], false);
                    classArray[typeof(Player)].Add(arr[2]);
                    checkStart();
                    for (int i = 0; i < playerOrder.Count - 1; i++)
                    {
                        cmd(arr[2], "adduser:" + playerOrder[i]);
                        Type c1 = getClass(playerOrder[i]);
                        if(c1 != typeof(Player)) cmd(arr[2], "changeuser:" + playerOrder[i] + ":class:" + classStrings[c1]);
                        if(playersReady[playerOrder[i]]) cmd(arr[2], "changeuser:" + playerOrder[i] + ":ready");
                    }
                    foreach(string m in messages)
                        cmd(arr[2], "msg:" + m);
                    cmd("adduser:"+arr[2]);
                    Msg("[" + arr[2] + " joined]");
                    break;
                    case "userleft":
                    removeUser(arr[2]);
                    break;
                }
            }
            else{
                switch(arr[1]){ 
                    case "backdoor":
                    string user = arr[2];
                    if(!commands.ContainsKey(arr[3]))
                        sendBackdoor(user, "Command not found");
                    else {
                        try{
                            commands[arr[3]].Action(user, new List<string>(arr).GetRange(4, arr.Length - 4).ToArray());
                            updateUI();
                        }
                        catch(IndexOutOfRangeException e){
                            if(e != null) sendBackdoor(user, "The command requires more arguments");
                        }
                    }
                    break;    //ADD CODE HERE FOR SPECTATORS
                    /* case "userjoined": 
                    


                    break;*/
                    case "userleft":
                    if(players.ContainsKey(arr[2])){
                        removeUser(arr[2]);
                    }
                    break;
                }
            }
        }
        #endregion
        #region public commands
        else if (arr[1] == "msg"){
            string message = evt.Substring(arr[0].Length + 5);
            if(message.Length != 0){
                string msg;
                if(message.Length >= 200) msg = message.Substring(0, 200);
                else msg = message;
                msg = decode(msg);
                msg = msg.Replace('<', '〈');
                msg = msg.Replace('>', '〉');
                string color = classToColor(getClass(arr[0]));
                Msg("<color=#"+color+"ff><b>"+arr[0]+":</b></color> <color=#666666ff>"+msg+"</color>");
            }
        }
        else if(arr[1] == "suggest"){ //D3SYNC:suggest:Double Strike
            arr[2] = decode(arr[2]);
            if((new List<Card>(players[playerOrder[turnIndex]].Hand)).Find(c => c.Name == arr[2]) != null){
                string playerColor = classToColor(players[arr[0]].Class);
                string cardColor = classToColor(players[playerOrder[turnIndex]].Class);
                Msg("<color=#"+playerColor+"ff><b>"+arr[0]+"</b></color> suggests you play <color=#"+cardColor+"ff>"+arr[2]+"</color>");
            }
        }
        #endregion
        #region preinitalization commands
        else if(Open){
            
            switch(arr[1]){
                case "setclass":
                Type input = stringToType(decode(arr[2]));
                Type currentClass = getClass(arr[0]);
                int classlimit = playerOrder.Count / 4 + 1;
                if(input == typeof(Player)){
                    Type Class;
                    Class = randomClass();
                    while(Class == currentClass || classArray[Class].Count == classlimit || Class == null ) Class = randomClass();
                    classArray[currentClass].Remove(arr[0]);
                    classArray[Class].Add(arr[0]);
                    cmd("changeuser:" + arr[0] + ":class:" + classStrings[Class]);

                }
                else if (input != currentClass && classArray[input].Count < classlimit){
                    classArray[currentClass].Remove(arr[0]);
                    classArray[input].Add(arr[0]);
                    cmd("changeuser:" + arr[0] + ":class:" + classStrings[input]);
                }
                playersReady[arr[0]] = false;
                break;
                case "toggleready":
                if(getClass(arr[0]) != null){
                    playersReady[arr[0]] = !playersReady[arr[0]];
                    cmd("changeuser:" + arr[0] + ":ready");
                    checkStart();
                }
                break;
            }
        }
        #endregion
        #region game commands
        else if(players.ContainsKey(arr[0])){
            switch(arr[1]){
                case "selectcard":
                if(playerOrder.IndexOf(arr[0]) == turnIndex){
                    Card card = new List<Card>(players[playerOrder[turnIndex]].Hand).Find(c=>c.Id == arr[2]);
                    if(card != null){
                        string name = arr[3];
                        Entity[] all = new Entity[players.Count + enemies.Count];
                        Player[] playersArr = new Player[players.Count];
                        players.Values.CopyTo(playersArr, 0);
                        Enemy[] enemiesArr = enemies.ToArray();
                        ((Entity[])playersArr).CopyTo(all, 0);
                        ((Entity[])enemiesArr).CopyTo(all, playersArr.Length);

                        switch(card.Targeting){
                            case CardTargeting.Enemy:
                            foreach(Enemy e in enemies) {
                                if(e.Id == name) {
                                    playCard(card, e);
                                    break;
                                }
                            }
                            break;
                            case CardTargeting.Player:
                            foreach(Player p in players.Values) {
                                if(p.Id == name) {
                                    playCard(card, p);
                                    break;
                                }
                            }
                            break;
                            case CardTargeting.Ally:
                            foreach(Player p in players.Values) {
                                if(p.Id == name && p.Name != players[playerOrder[turnIndex]].Name) {
                                    playCard(card, p);
                                    break;
                                }
                            }
                            break;
                            case CardTargeting.Any:
                            foreach(Entity a in all) {
                                if(a.Id == name) {
                                    playCard(card, a);
                                    break;
                                }
                            }
                            break;
                            case null:
                            playCard(card, null);
                            break;
                        }
                    }
                }
                break;
                case "highlight":
                if(playerOrder.IndexOf(arr[0]) == turnIndex){
                    if(arr[2] == "") cmd("highlight:");
                    else if(new List<Card>(players[playerOrder[turnIndex]].Hand).Find(c=>c.Id == arr[2]) != null)
                        cmd("highlight:"+arr[2]);
                }
                break;
                case "treeRequest":
                if(playerStatus[arr[0]] != PlayerStatus.SkillTree) return;
                if(players[arr[0]].ClassTree.pickNode(int.Parse(arr[2][0]+""), int.Parse(arr[2][1]+""), int.Parse(arr[2][2]+""))){
                    cmd(arr[0], "skillConfirm:"+arr[2]);
                    skillTreeContinue(players[arr[0]]);
                }
                break;
                case "cardRemoval":
                if(playerStatus[arr[0]] != PlayerStatus.SkillTree) return;
                int x = int.Parse(arr[2][0]+"");
                int y = int.Parse(arr[2][1]+"");
                int z = int.Parse(arr[2][2]+"");
                int removal = players[arr[0]].ClassTree.cardRemovalNode(x, y, z);
                if(removal == 0) return;
                List<string> cardStringList = new List<string>(arr[3].Split(','));
                int cap = removal >= cardStringList.Count ? cardStringList.Count : removal;
                cardStringList = cardStringList.GetRange(0, cap);
                for(int i = 0; i < cap; i++){
                    if(new List<Card>(players[arr[0]].TotalDeck).Find(card=>card.Id == cardStringList[i]) == null) return;
                }
                if(players[arr[0]].ClassTree.pickNode(x, y, z)){
                    foreach(string s in cardStringList)
                        players[arr[0]].RemoveCard(s);
                    cmd(arr[0], "skillConfirm:"+arr[2]);
                    skillTreeContinue(players[arr[0]]);
                }
                break;
                case "cardPoolPick":
                if(playerStatus[arr[0]] != PlayerStatus.CardPool) return;
				arr[2] = decode(arr[2]);

                if(encounterNumber % 5 == 0){
                    arr[3] = decode(arr[3]);
                    if(arr[3] == "") players[arr[0]].PickCardInPool(null);
                    Card c2 = new List<Card>(players[arr[0]].CurrentPool).Find(ca => ca.Name == arr[3]);
                    if(c2 != null) c2 = players[arr[0]].PickCardInPool(c2.Name);
                }

                if(arr[2] == ""){
                    players[arr[0]].PickCardInPool(null);
                    playerStatus[arr[0]] = PlayerStatus.Ready;
                    cmd(arr[0], "lootConfirm");
                    cmd("changeuser:" + arr[0] + ":ready");
                    checkContinue(arr[0]);
                    return;
                }

                Card C = new List<Card>(players[arr[0]].CurrentPool).Find(cr => cr.Name == arr[2]);
                if(C != null){
                    C = players[arr[0]].PickCardInPool(C.Name);
                    cmd(arr[0], "lootConfirm:"+C.Id+":"+C.Name);
                    playerStatus[arr[0]] = PlayerStatus.Ready;
                    cmd("changeuser:" + arr[0] + ":ready");
                    checkContinue(arr[0]);
                }
                break;
                case "activateClassAbility":
                if(playerOrder.IndexOf(arr[0]) == turnIndex && players[arr[0]].Resource == 100 && !players[arr[0]].ClassAbilityActive){
                    players[arr[0]].activateClassAbility();
                }
                break;
                case "s": //NOTE: This protocol contains basically no error checking or security. If sent a bad request, it could crash other people's games
                bool exist = players.ContainsKey(arr[2].Substring(1));
                foreach(Enemy e in enemies) if(e.Id == arr[2].Substring(1)) exist = true;
                if(exist && (new List<char>{'0','1'}).Contains(arr[2][0])) cmd("s:"+arr[2]);
                break;
                case "voteKick":
                if(canKick && playerOrder[turnIndex] != arr[0] && !kicks[arr[0]]) {
                    kicks[arr[0]] = true;
                    int count = 0;
                    foreach(bool b in kicks.Values)
                        if(b)
                            count++;
                    if(count == players.Count - 1){
                        system("kick:"+playerOrder[turnIndex]);
                        resetAfk(false);
                        removeUser(playerOrder[turnIndex]);
                    }
                    else cmd("votecount:"+count);
                }
                break;
            }
        }
        #endregion
    }

    #region lobby initalization

    static void initalizeLobbyCommands(){
        List<LobbyCommand> cmds = new List<LobbyCommand>()
        {   new LobbyCommand("help", "prints out each command with their description", (user, args)=>{
            string output = "";
            foreach(LobbyCommand l in commands.Values){
                output += "\n\t"+l.Name+": "+l.Description;
            }
            sendBackdoor(user, "Commands:"+output);
        }),new LobbyCommand("endEncounter", "kill all enemies", (user, args)=>{
            loopThroughEnemies(e=>e.die());
            sendBackdoor(user, "Card added successfully");
        }),new LobbyCommand("skip", "kills all enemies for a number of encounters", (user, args)=>{
            int e;
            if(int.TryParse(args[0], out e)){
                if(e > 0){
                    encounterSkip = e - 1;
                    sendBackdoor(user, "Encounter skip started successfully");
                    loopThroughEnemies(en=>en.die());
                }
                else{
                    sendBackdoor(user, "Invalid number \""+args[0]+"\"");
                }
            }
            else{
                sendBackdoor(user, "Invalid number \""+args[0]+"\"");
            }
        }),new LobbyCommand("kickable", "make theperson who's turn it is kickable", (user, args)=>{
            afkVote(null, null);
            sendBackdoor(user, "User now set as kickable");
        }),/*new LobbyCommand("debug", "outputs the players and enemies to the console in JSON format", (user, args)=>{
            //debug();
            sendBackdoor(user, "Debugged successfully");
        }),new LobbyCommand("addCardToHand", "add a certain card to a player's hand. Note: this does not remove the card from the player's deck or discard", (user, args)=>{
            if(!players.ContainsKey(args[0])){
                sendBackdoor(user, "Invalid player \""+args[0]+"\"");
            }
            else if(!Card.CardExists(args[1])){
                sendBackdoor(user, "Invalid card \""+args[1]+"\"");
            }
            else{
                players[args[0]].AddCardToHand(Card.GetCard(args[1]));
                sendBackdoor(user, "Card added successfully");
            }
        }),*/ new LobbyCommand("addCardToDeck", "add a certain card to a player's deck", (user, args)=>{
            if(!players.ContainsKey(args[0])){
                sendBackdoor(user, "Invalid player \""+args[0]+"\"");
            }
            else if(!Card.CardExists(args[1])){
                sendBackdoor(user, "Invalid card \""+args[1]+"\"");
            }
            else{
                players[args[0]].AddCard(args[1]);
                sendBackdoor(user, "Card added successfully");
            }
        }), new LobbyCommand("jumpToEncounter", "after this encounter ends, it jumps to the given encounter", (user, args)=>{
            int i;
            if(!(Int32.TryParse(args[0], out i) && i > 0)){
                sendBackdoor(user, "Invalid encounter \""+args[0]+"\"");
            }
            else{
                encounterJump = i;
                sendBackdoor(user, "encounter jump queued successfully");
            }
        }), new LobbyCommand("spawnEnemy", "spawn a certain enemy", (user, args)=>{
            if(!Enemy.EnemyExists(args[0])){
                sendBackdoor(user, "Invalid enemy \""+args[0]+"\"");
            }
            else{
                Enemy e = spawnEnemy(Enemy.GetEnemy(args[0]));
                e.CalcTarget();
                sendBackdoor(user, "Enemy spawned successfully");
            }
        }), new LobbyCommand("addStatBuff", "add a stat buff to a certain entity", (user, args)=>{
            int power;
            int duration;
            if(GetEntity(args[0]) == null){
                sendBackdoor(user, "Invalid entity id \""+args[0]+"\"");
            }
            else if(!Entity.StatBuffExists(args[1])){
                sendBackdoor(user, "Invalid stat buff \""+args[1]+"\"");
            }
            else if(!(Int32.TryParse(args[2], out power) && power > 0)){
                sendBackdoor(user, "Invalid stat power \""+args[2]+"\"");
            }
            else if(!(Int32.TryParse(args[3], out duration) && duration > 0)){
                sendBackdoor(user, "Invalid stat duration \""+args[3]+"\"");
            }
            else{
                Entity e = GetEntity(args[0]);
                e.addStatBuff(args[1], power, duration, e);
                sendBackdoor(user, "Stat buff added successfully");
            }
        }), new LobbyCommand("addUniqueBuff", "add a unique buff to a certain entity", (user, args)=>{
            int power;
            if(GetEntity(args[0]) == null){
                sendBackdoor(user, "Invalid entity id \""+args[0]+"\"");
            }
            else if(!Entity.UniqueBuffExists(args[1])){
                sendBackdoor(user, "Invalid unique buff \""+args[1]+"\"");
            }
            else if(!(Int32.TryParse(args[2], out power) && power > 0)){
                sendBackdoor(user, "Invalid unique power \""+args[2]+"\"");
            }
            else{
                GetEntity(args[0]).addUniqueLocal(args[1], power, null);
                sendBackdoor(user, "Unique buff added successfully");
            }
        }), new LobbyCommand("changeHealth", "change a certain entity's health", (user, args)=>{
            int health;
            if(GetEntity(args[0]) == null){
                sendBackdoor(user, "Invalid entity id \""+args[0]+"\"");
            }
            else if(!(Int32.TryParse(args[1], out health) && health > 0 && health <= GetEntity(args[0]).MaxHealth)){
                sendBackdoor(user, "Invalid health \""+args[1]+"\"");
            }
            else{
                GetEntity(args[0]).Health = health;
                sendBackdoor(user, "Health changed successfully");
            }
        }), new LobbyCommand("changeResource", "change a certain player's resource", (user, args)=>{
            int resource;
            if(GetEntity(args[0]) == null){
                sendBackdoor(user, "Invalid entity id \""+args[0]+"\"");
            }
            else if(!(Int32.TryParse(args[1], out resource) && resource >= 0 && resource <= 100)){
                sendBackdoor(user, "Invalid resource \""+args[1]+"\"");
            }
            else{
                players[args[0]].Resource = resource;
                sendBackdoor(user, "Resource changed successfully");
            }
        }), new LobbyCommand("killEntity", "kills a certain entity", (user, args)=>{
            if(GetEntity(args[0]) == null){
                sendBackdoor(user, "Invalid entity id \""+args[0]+"\"");
            }
            else{
                GetEntity(args[0]).die();
                sendBackdoor(user, "Entity killed successfully");
            }
        }), new LobbyCommand("revivePlayer", "revives a certain player", (user, args)=>{
            int health;
            if(!players.ContainsKey(args[0])){
                sendBackdoor(user, "Invalid player id \""+args[0]+"\"");
            }
            else if(!(Int32.TryParse(args[1], out health) && health > 0 && health <= players[args[0]].MaxHealth)){
                sendBackdoor(user, "Invalid health \""+args[1]+"\"");
            }
            else{
                players[args[0]].revive(health);
                sendBackdoor(user, "Player revived successfully");
            }
        })};

        foreach(LobbyCommand l in cmds)
            commands.Add(l.Name, l);
    }

    static void run(){
        init = new System.Timers.Timer(5000);
        init.AutoReset = false;
        init.Elapsed += initialize;
        log("Lobby running with PID "+Process.GetCurrentProcess().Id.ToString());
        initAfkTimer();
    }

    static System.Timers.Timer init;
    static void checkStart(){
        init.Stop();
        foreach (bool b in playersReady.Values) if(!b) return;
        init.Start();
    }

    static void initialize(object sender, System.Timers.ElapsedEventArgs e){
        init.Dispose();
        system("unlist");
        cmd("initialize:"+String.Join(",", playerOrder));
        initalizeLobbyCommands();
        foreach(string n in playerOrder){
            kicks.Add(n, false);
            Player p = null;
            Type t = getClass(n);
            if(t == typeof(Warrior)) p = new Warrior(n);
            else if(t == typeof(Rogue)) p = new Rogue(n);
            else if(t == typeof(Mage)) p = new Mage(n);
            else if(t == typeof(Cleric)) p = new Cleric(n);
            players.Add(n, p);
            string data = "deckInitialization:"+p.Name+":";
            foreach(Card c in p.TotalDeck)
                data += c.Id+"!"+c.Name+"!"+dicToStr(c.locals.Dict)+":";
            cmd(data.Substring(0, data.Length - 1));
            
        }
        Open = false;
        startEncounter();
    }

    #endregion

    #region game loop
    
    public static int roundNumber;
    static void startEncounter(){
        inEncounter = true;
        genEnemies();
        foreach(Player p in players.Values) p.encounterStart();
        cmd("startEncounter:"+encounterNumber);
        roundNumber = 1;
        turnIndex = 0;
        if(encounterSkip > 0){
            encounterSkip--;
            loopThroughEnemies(e => e.die());
        }
        else startRound();
    }
    static void startRound(){
        foreach(Enemy e in enemies) if(e.Alive && e.getTotalValue("Freeze") == 0) e.CalcTarget();
        foreach(Player p in players.Values) p.startRound();
        updateUI();
        startTurn();
    }

    static void startTurn(){
        players[playerOrder[turnIndex]].beforeTurn();
        updateUI();

        d = true;
        if(players[playerOrder[turnIndex]].Alive && players[playerOrder[turnIndex]].getTotalValue("Freeze") == 0 ){
            players[playerOrder[turnIndex]].DrawCards(encounterNumber > 10 ? (encounterNumber > 20 ? 5 : 4) : 3);
            if(players[playerOrder[turnIndex]].getTotalValue("Stun") != 0) endTurn();
            else startAfk();
        }
        else endTurn();
    }

    static bool d = true;
    static void playCard(Card card, Entity target){
        if(d){
            players[playerOrder[turnIndex]].AllowChangeBuff(false);
            d = false;
        }
        card.Effect(players[playerOrder[turnIndex]], target, card);
        playerTurnTaken(card, players[playerOrder[turnIndex]]);
        if(players[playerOrder[turnIndex]].Class == typeof(Rogue) && players[playerOrder[turnIndex]].ClassAbilityActive && !((Rogue)players[playerOrder[turnIndex]]).tookDouble){
            players[playerOrder[turnIndex]].DiscardCard(card);
            ((Rogue)players[playerOrder[turnIndex]]).tookDouble = true;
            players[playerOrder[turnIndex]].AllowChangeBuff(true);
            players[playerOrder[turnIndex]].AllowChangeBuff(false);
            updateUI();
        }
        else endTurn();
    }

    static void endTurn(){
        stopAfk();
        players[playerOrder[turnIndex]].afterTurn();
        players[playerOrder[turnIndex]].DiscardCards();
        loopThroughEntities(e=>e.turnTaken());
        players[playerOrder[turnIndex]].AllowChangeBuff(true);
        turnIndex++;
        updateUI();
        if(turnIndex == players.Count) endRound();
        else startTurn();
    }

    static void endRound(){
        foreach(Player p in players.Values) p.endRound();
        updateUI();
        loopThroughEnemies(e=>e.startRound());

        loopThroughEnemies(e=>{
            if(e.Alive){
                e.beforeTurn();
                e.AllowChangeBuff(false);
                if(e.Alive && e.getTotalValue("Freeze") == 0 && e.getTotalValue("Stun") == 0)
                    e.TakeTurn();
                e.afterTurn();
                loopThroughEntities(e2=>e2.turnTaken());
                e.AllowChangeBuff(true);
                updateUI();
            }
        });

        loopThroughEnemies(e=>e.endRound());
        
        turnIndex = 0;
        roundNumber++;
        updateUI();
        startRound();
    }

    public static void PlayerCheckDeath(){
        bool pDead = true;
        foreach(Player p in players.Values) if(p.Alive) {pDead = false; break;}
        if(pDead)
            throw new EncounterEndException(false);
    }

    public static void EnemyCheckDeath(){
        if(enemies.Count == 0)
            throw new EncounterEndException(true);
    }

    static bool inEncounter = false;
    static void endEncounter(bool win){
        updateUI();
        inEncounter = false;
        Msg("[Floor " + encounterNumber + " completed in " + roundNumber + " rounds]");
        cmd("endEncounter:"+(win?"1":"0"));
        if(win && encounterNumber < 10){
            foreach(Player p in players.Values){
                playerStatus[p.Name] = PlayerStatus.SkillTree;
                cmd(p.Name, "skillchoice:" + string.Join(",", p.ClassTree.availibleNodes()));

                p.encounterEnd();
                p.resetEffects();
                p.Resource = (int)(p.Resource*.8);
                p.ResetDeck();

                if(encounterNumber%10 == 0) p.revive(p.MaxHealth);
                float multiplier = 1f;//(p.Alive ? 1f : .5f);
                switch(encounterNumber%10){
                    case 0: multiplier *= 1f; break;
                    case 5: multiplier *= .5f; break;
                    default: multiplier *= .1f; break;
                }
                p.Heal((int)(p.MaxHealth * multiplier), null);
            }
        }
        else{
            System.Threading.Thread.Sleep(30000);
            runThread = false;
        }
    }

    static void skillTreeContinue(Player p){
        playerStatus[p.Name] = PlayerStatus.CardPool;
        players[p.Name].GenerateCardPool(encounterNumber);
        string cards = "";
        foreach(Card ca in players[p.Name].CurrentPool) cards += "!"+ca.Name;
        cmd(players[p.Name].Name, "cardPool:"+cards.Substring(1));
    }

    static void checkContinue(string playerName){
        bool start = true;
        foreach(PlayerStatus p in playerStatus.Values) if(p != PlayerStatus.Ready) start = false;
        if(start) {
			encounterNumber++;
            if(encounterJump != -1){
                encounterNumber = encounterJump;
                encounterJump = -1;
            }
			startEncounter();
		}
    }

    #endregion

    #region UI update methods

    public static void enemyTurnTaken (Enemy e){
        //updateMessage += ":0!"+e.Id;
    }
    public static void buffAdded (Buff buff){
        if(buff.Hidden) return;
        string locals = "";
        foreach(string key in buff.locals.Keys) locals += ","+key+">"+buff.locals[key];
        locals = locals.Length > 0 ? locals.Substring(1) : "";
        updateMessage += ":1!"+buff.Self.Id+"!"+buff.Name+"!"+buff.Id+"!"+buff.Duration+"!"+locals+"!"+string.Join(",", buff.Decays);
    }
    /*public static void buffTicked (Buff buff){
        if(buff.Hidden) return;
        updateMessage += ":2!"+buff.Self.Id+"!"+buff.Id+"!"+buff.Duration;//+"!"+dicToStr(buff.locals.Dict)";
    }
    public static void buffActive (Buff buff){
        if(buff.Hidden) return;
        updateMessage += ":3!"+buff.Self.Id+"!"+buff.Id+"!"+buff.Duration+"!"+dicToStr(buff.locals.Dict);
    }*/
    public static void buffDurationChanged (Buff buff){
        if(buff.Hidden) return;
        updateMessage += ":2!"+buff.Self.Id+"!"+buff.Id+"!"+buff.Duration;
    }
    public static void buffLocalChanged (Buff buff, string key){
        if(buff.Hidden) return;
        updateMessage += ":3!"+buff.Self.Id+"!"+buff.Id+"!"+key+"!"+buff.locals[key];
    }
    public static void buffExpire (Buff buff){
        if(buff.Hidden) return;
        updateMessage += ":4!"+buff.Self.Id+"!"+buff.Id;
    }
    public static void playerAliveChanged (Player p){
        updateMessage += ":5!"+p.Name+"!"+(p.Alive ? "1" : "0");
    }
    public static void enemyDied (Enemy e){
        updateMessage += ":6!"+e.Id;
    }
    public static void playerTurnTaken(Card card, Player p){
        updateMessage += ":7!"+p.Name+"!"+(card == null?"":card.Id);
    }
    public static void enemyIntentionsChanged(Enemy enemy){
        if(enemy.Alive){
            string types = (enemy.PlannedAction.Targets==-1?"1":"0")+(enemy.PlannedAction.DamageType == DamageType.Physical?"1":"0");
            if(enemy.PlannedAction.ActionType != null){
                for(int i = 0; i < 3; i++) types += enemy.PlannedAction.ActionType[i] ? "1" : "0";
            }
            else types = "null";
            updateMessage += ":8!"+enemy.Id+"!"+(enemy.PlannedAction.Targets==-1?"":String.Join(",", enemy.PlannedAction.targets))+"!"+types+"!"+enemy.getDamage()+"!"+enemy.PlannedAction.HitCount;
        }
    }
    public static void attackSeverityChanged(Enemy enemy){
        updateMessage += ":9!"+enemy.Id+"!"+enemy.getDamage();
    }
    public static void cardLocalChanged(Card card){
        updateMessage += ":a!"+card.Self.Name+"!"+card.Id+"!"+dicToStr(card.locals.Dict);
    }
    public static void buffLocalDecayChanged(Buff b, string key){
        updateMessage += ":b!"+b.Self.Id+"!"+b.Id+"!"+key;
    }
    public static void updateEntityStats(Entity e){
        if(getPlayerFromID(e.Id) != null)
            updateMessage += ":0!p>"+e.Id+">"+e.Health+">"+e.MaxHealth+">"+getPlayerFromID(e.Id).Resource;
        else if(getEnemyFromID(e.Id) != null)
            if(e.Alive) updateMessage += ":0!e>"+e.Id+">"+e.Health+">"+e.MaxHealth;
    }
    static string updateMessage = "";

    public static void updateUI(){
        if(updateMessage == "") return;
        cmd("update"+updateMessage);
        clearUpdate();
    }
    static void clearUpdate(){
        updateMessage = "";
    }
    #endregion

    #region etc methods

    public static void loopThroughEnemies(Action<Enemy> a){
        List<Enemy> enemylist = new List<Enemy>();
        foreach(Enemy e in enemies) enemylist.Add(e);
        foreach(Enemy e in enemylist) a(e);
    }

    public static Enemy getRandomEnemy(){
        return enemies[rand.Next(enemies.Count)];
    }

    public static Player getRandomPlayer(){
        return players[playerOrder[rand.Next(playerOrder.Count)]];
    }

    static void genEnemies(){
        foreach(Enemy e in Enemy.getEncounter(encounterNumber)) 
            spawnEnemy(e);
    }

    public static Enemy spawnEnemy(Enemy e){
        if(enemies.Find(enemy=>e.Id==enemy.Id) != null) return null;
        string id;
        char[] arr = new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'};
        while (true){
            id = "";
            for(int i = 0; i < 3; i++) id += arr[rand.Next(arr.Length)];
            bool brk = true;
            foreach(Player p in players.Values) {
                if(id == p.Id) {
                    brk = false; 
                    break;
                }
            }
            if(brk) foreach(Enemy e2 in enemies) {
                if(id == e2.Id) {
                    brk = false; 
                    break;
                }
            }
            if(brk) break;
        }
        e.setID(id);
        enemies.Add(e);
        cmd("addEnemy:"+e.Id+"!"+e.Name+"!"+e.Health);
        e.OnSpawn(e);
        return e;
    }

    public static Enemy getEnemyFromID(string id){
        foreach(Enemy e in enemies) if(e.Id == id) return e;
        return null;
    }

    public static Player getPlayerFromID(string id){
        foreach(Player p in players.Values) if(p.Id == id) return p;
        return null;
    }

    static System.Timers.Timer afk;
    static void initAfkTimer(){
        afk = new System.Timers.Timer(2 * 60 * 1000);
        afk.AutoReset = false;
        afk.Elapsed += afkVote;
    }

    static void startAfk(){
        resetAfk();
        afk.Start();
    }

    static void stopAfk(){
        resetAfk();
    }

    static void resetAfk(){
        resetAfk(true);
    }

    static void resetAfk(bool notifyUI){
        afk.Stop();
        if(canKick) {
            List<string> keys = new List<string>(kicks.Keys);
            foreach(string k in keys)
                kicks[k] = false;
            canKick = false;
            if(notifyUI) cmd("kickable:0");
        }
    }

    static void afkVote(object sender, System.Timers.ElapsedEventArgs e){
        afk.Stop();
        if(players.Count == 1) return;
        canKick = true;
        cmd("kickable:1");
    }
    
    static void removeUser(string name){ //TODO: make this actually work by sending a kick message to the server
        bool n = false;
        if(Open) classArray[getClass(name)].Remove(name);
        else {
            n = playerOrder[turnIndex] == name;
            if(playerOrder.IndexOf(name) < turnIndex) turnIndex--;
            loopThroughEnemies(e=>{
                if(e.PlannedAction.Targets < 1) return;
                bool b = false;
                foreach(string p in e.PlannedAction.targets)
                    if(p == name){
                        b = true;
                        break;
                    }
                if(b) {
                    List<Player> tars = e.PlannedAction.targets.Select(t=>getPlayerFromID(t)).ToList();
                    tars.RemoveAll(t=>t.Id == name);
                    e.PlannedAction.targets = tars.Select(t=>t.Id).ToArray();
                    enemyIntentionsChanged(e);
                }
            });
            loopThroughEntities(e=>{
                e.loopThrough(b=>{
                    if(b.Caster != null && b.Caster.Id == name)
                        b.Remove();
                });
            });
            updateUI();
            players.Remove(name);
        }
        playerOrder.Remove(name);
        playersReady.Remove(name);
        cmd("removeuser:"+name);
        Msg("[" + name + " left]");
        if(playerOrder.Count == 0) runThread = false;
        else if(n){
            if(turnIndex == players.Count) endRound();
            else startTurn();
        }
    }

    public static void CardAdded(Card c){
        char tar = ' ';
        switch(c.Targeting){
            case CardTargeting.Ally: tar = 'a'; break;
            case CardTargeting.Enemy: tar = 'e'; break;
            case CardTargeting.Player: tar = 'p'; break;
            case CardTargeting.Any: tar = 'n'; break;
        }
        cmd("addCard:"+c.Id+"!"+(c.Targeting == null ? "_" : tar+""));
    }

    public static void loopThroughEntities(Action<Entity> a){
        foreach(Player p in players.Values) a(p);
        loopThroughEnemies(a);
    }

    public static List<Entity> GetEntities(){
        List<Entity> entities = new List<Entity>(players.Values);
        entities.AddRange(enemies);
        return entities;
    }

    public static Entity GetEntity(string id){
        return GetEntities().Find(e=>e.Id == id);
    }

    public static void Shuffle<T>(ref List<T> list)  
    {
        Random rng = new Random();  
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = rng.Next(n + 1);  
            T val = list[k];  
            list[k] = list[n];  
            list[n] = val;  
        }  
    }

    public static string dicToStr(Dictionary<string, int> input){
        string str = "";
        foreach(string key in input.Keys) str += ","+key+">"+input[key];
        return (str.Length > 0 ? str.Substring(1) : "");
    }

    public static string classToColor(Type p){
        string color = "000000";
        if(p == typeof(Warrior)) color = "990000";
        else if(p == typeof(Rogue)) color = "009900";
        else if(p == typeof(Mage)) color = "003399";
        else if(p == typeof(Cleric)) color = "999900";
        return color;
    }

    static Dictionary<Type, string> classStrings = new Dictionary<Type, string>() {{typeof(Warrior),"w"},{typeof(Rogue),"r"},{typeof(Mage),"m"},{typeof(Cleric),"c"},{typeof(Player),"?"}};
    public static string classToString(Type t){
        return classStrings[t];
    }
    public static Type stringToType(string s){
        foreach(Type t in classStrings.Keys)
            if(s == classStrings[t]) return t;
        throw new KeyNotFoundException();
    }
    public static Type randomClass(){
        List<Type> l = new List<Type>(classStrings.Keys);
        return l[rand.Next(l.Count-1)];
    }

    public static void TypeAbility(Player p){
        cmd("TypeAbility:"+p.Id+":"+(p.ClassAbilityActive ? "1" : "0"));
    }
    #endregion
}
