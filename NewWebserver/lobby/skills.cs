using System.Collections.Generic;
using System.Collections;
using System;

namespace classes{
    public partial class Warrior{
        public override void initalizeSkillTree(){
            AddCards(new string[] {"Strike", "Strike", "Strike", "Stand Ground", "Stand Ground", "Stand Ground", "Bash", "Strengthen", "Double Strike", "Toughen"});
            AddToStarterPool(new string[] {"Bash", "Strengthen", "Double Strike", "Toughen"});
            skillTree = new StandardSkillTree(this, new Action[][][]{ //warrior skill tree
                #region section 0
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{
                            AddEncounterEffect(()=>{
                                addStatBuff("Strong", "Strength", 1, -1, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Heal(5, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                addStatBuff("Fortified", "Shield", 10, 3, this);
                            });
                        }, ()=>{
                            MaxHealth += 10;
                            Health += 10;
                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{
                            AddToBeginnerPool(new string[]{"Cleave", "Reckless Strike", "Frenzied Strikes", "Strengthened Strike", "Scarred Strength"});
                        }, ()=>{
                            AddToBeginnerPool(new string[]{"Defensive Strike", "Swordbreaker", "Sanguine Strike", "Against the Odds", "Last Stand"});
                        }, ()=>{
                            AddToBeginnerPool(new string[]{"Insulting Presence", "Defensive Taunt", "Unbreakable", "Bulwark", "Blood Armor"});
                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Furyborn", -1, null, this, true);
                                b.locals.Add("Strength", 0);
                                b.AddActivation(new BeforeTurn(()=>{
                                    float p = ((float)b.Self.Health) / ((float)b.Self.MaxHealth);
                                    if(p > .75){
                                        b.locals["Strength"] = 0;
                                        b.Hidden = true;
                                    }
                                    else if(p > .5){
                                        b.locals["Strength"] = 1;
                                        b.Hidden = false;
                                    }
                                    else if(p > .25){
                                        b.locals["Strength"] = 2;
                                        b.Hidden = false;
                                    }
                                    else{
                                        b.locals["Strength"] = 3;
                                        b.Hidden = false;
                                    }
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                           AddEncounterEffect(()=>{
                                Buff b = new Buff("Taste of Blood", -1, null, this);
                                b.AddActivation(new AfterAttack((damage, target, Dtype, Atype)=>{
                                    if(target.Health == 0 && damage > 0)
                                        b.Self.Heal(5, b.Self);
                                }));
                                addBuff(b);
                            }); 
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Spiked Armor", -1, null, this);
                                b.AddActivation(new AfterDefend((damage, attacker, dType, Atype)=>{
                                    if(Atype == AttackType.Base || Atype == AttackType.AoE) attacker.Defend(null, 2, DamageType.Physical, AttackType.Base);
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Unarmored Defense", -1, null, this, true);
                                b.AddActivation(new EndRound(()=>{
                                    if(b.Self.getTotalValue("Shield") == 0)
                                        b.Self.addStatBuff("Unarmored Defense", "Shield", 5, 1, b.Self);
                                }));
                                addBuff(b);
                            });
                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{
                            AddCard("Flurry of Blows");
                        }, ()=>{
                            AddCard("Culling");
                        }, ()=>{
                            AddCard("Barricade");
                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 1
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 2
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 3
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 4
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 5
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 6
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                }
                #endregion
            });
        }
    }

    public partial class Rogue{
        public override void initalizeSkillTree(){
            AddCards(new string[] {"Double Slash", "Double Slash", "Double Slash", "Sneak", "Sneak", "Sneak", "Acidic Poison", "Flurry Slash", "Tripwire", "Preparation"});
            AddToStarterPool(new string[] {"Acidic Poison", "Flurry Slash", "Tripwire", "Preparation"});
            skillTree = new StandardSkillTree(this, new Action[][][]{ //warrior skill tree
                #region section 0
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{
                            AddEncounterEffect(()=>{
                                addUniqueLocal("Daggers", 3, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Lobby.enemies[rand.Next(Lobby.enemies.Count)].addUniqueLocal("Poison", 4, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                addStatBuff("Stealthed", "Dodge", 1, 2, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Alert", 1, typeof(AfterTurn), this);
                                b.AddActivation(new BeforeTurn(()=>{
                                    Lobby.getPlayerFromID(b.Self.Id).DrawCards(1);
                                }));
                                addBuff(b);
                            });
                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{
                            AddToBeginnerPool(new string[]{"Cloak and Dagger", "Deadly Acrobat", "Sharpen", "Poison Daggers", "Piercing Daggers"});
                        }, ()=>{
                            AddToBeginnerPool(new string[]{"Plague", "Blinding Poison", "Paralyzing Poison", "Snakebite Slash", "Assassin's Technique"});
                        }, ()=>{
                            AddToBeginnerPool(new string[]{"Mocking Shadows", "Bear Trap", "Blinding Trap", "Poison Trap", "Heads Up"});
                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Sharp", -1, null, this);
                                b.AddActivation(new OnAttack((damage, target, Dtype, Atype)=>{
                                    if(Atype == AttackType.Daggers)
                                        damage++;
                                    return damage;
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Toxic", -1, null, this);
                                b.locals.Add("poisonable", 1);
                                b.AddActivation(new AfterUniqueLocalAdded((key, duration, caster, target)=>{
                                    if(caster == b.Self && key == "Poison" && b.locals["poisonable"] == 1){
                                        b.locals["poisonable"] = 0;
                                        target.addUniqueLocal("Poison", 1, b.Self);
                                    }
                                }));
                                b.AddActivation(new BeforeTurn(()=>{
                                    b.locals["poisonable"] = 1;
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Cunning", -1, null, this);
                                b.AddActivation(new AfterBuffApplied((buff)=>{
                                    if(buff.Caster == b.Self && buff.BuffType == BuffClass.Trap)
                                        buff.Duration++;
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Focused", -1, null, this);
                                b.locals.Add("damage", 0);
                                b.AddActivation(new AfterDefend((damage, attacker, Dtype, Atype)=>{
                                    b.locals["damage"] += damage;
                                }));
                                b.AddActivation(new EndRound(()=>{
                                    if(b.locals["damage"] == 0)
                                        Lobby.getPlayerFromID(b.Self.Id).Resource += 2;
                                    b.locals["damage"] = 0;
                                }));
                                addBuff(b);
                            });
                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{
                            AddCard("Tempest");
                        }, ()=>{
                            AddCard("Infect");
                        }, ()=>{
                            AddCard("Shadow");
                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 1
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 2
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 3
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 4
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 5
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 6
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                }
                #endregion
            });
        }
    }

    public partial class Mage{
        public override void initalizeSkillTree(){
            AddCards(new string[] {"Aether Bolt", "Aether Bolt", "Aether Bolt", "Aether Barrier", "Aether Barrier", "Aether Barrier", "Explosion", "Curse", "Arcane Charge", "Enhance Arcana"});
            AddToStarterPool(new string[] {"Explosion", "Curse", "Arcane Charge", "Enhance Arcana"});
            skillTree = new StandardSkillTree(this, new Action[][][]{ //warrior skill tree
                #region section 0
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{
                            AddEncounterEffect(()=>{
                                addStatBuff("Intelligent", "Intellect", 1, -1, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Enemy e = Lobby.enemies[rand.Next(Lobby.enemies.Count)];
                                e.addStatBuff("Armor", -2, 3, this);
                                e.addStatBuff("Resolve", -2, 3, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                addUniqueLocal("Burst", 4, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                this.addBuff(new Buff("Arcane Shield", 1, typeof(EndRound), this)).AddActivation(new StartRound(()=>{
                                    Buff buff = Entity.createStatBuff("Aether Barrier", "Shield", 10, 1, this);
                                    buff.AddActivation(new OnDamage( (damage) => {
                                        this.Resource += damage > buff.locals["Shield"] ? buff.locals["Shield"] : damage;
                                        return damage;
                                    }));
                                    buff.AddEffect("Shield");
                                    this.addBuff(buff);
                                }));
                            });
                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{
                            AddToBeginnerPool(new string[]{"Magic Missiles", "Ancient Knowledge", "Strange Brew", "Meditate", "Arcane Armor"});
                        }, ()=>{
                            AddToBeginnerPool(new string[]{"Death Wish", "Aether Charm", "Venom Mark", "Necrotic Blast", "Rend"});
                        }, ()=>{
                            AddToBeginnerPool(new string[]{"Aether Reaver", "Maelstrom", "Fracture", "Flash Bomb", "Blast Shield"});
                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Peak Condition", -1, null, this, true);
                                b.locals.Add("Intellect", 0);
                                b.AddActivation(new StartRound(()=>{
                                    float p = ((float)b.Self.Health) / ((float)b.Self.MaxHealth);
                                    if(p < .25){
                                        b.locals["Intellect"] = 0;
                                        b.Hidden = true;
                                    }
                                    else if(p < .5){
                                        b.locals["Intellect"] = 1;
                                        b.Hidden = false;
                                    }
                                    else if(p < .75){
                                        b.locals["Intellect"] = 2;
                                        b.Hidden = false;
                                    }
                                    else{
                                        b.locals["Intellect"] = 3;
                                        b.Hidden = false;
                                    }
                                }));
                                b.AddEffect("Intellect");
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Spell Bane", -1, null, this);
                                b.AddActivation(new AfterBuffApplied((buff)=>{
                                    if(buff.Caster == b.Self && buff.BuffType == BuffClass.Hex)
                                        buff.Duration++;
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Arcane Burst", -1, null, this);
                                b.AddActivation(new AfterUniqueLocalAdded((key, val, caster, target)=>{
                                    if(target == b.Self && caster == b.Self && key == "Burst")
                                        target.addUniqueLocal("Burst", 2, null);
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Orb of Aether", -1, null, this);
                                b.locals.Add("aether", 1);
                                b.AddActivation(new OnResourceChange((res)=>{
                                    if(res > 0 && b.locals["aether"] == 1){
                                        res+=2;
                                        b.locals["aether"] = 0;
                                    }
                                    return res;
                                }));
                                b.AddActivation(new BeforeTurn(()=>{
                                    b.locals["aether"] = 1;
                                }));
                                addBuff(b);
                            });
                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{
                            AddCard("Devour Intellect");
                        }, ()=>{
                            AddCard("Scourge");
                        }, ()=>{
                            AddCard("Nova Charge");
                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 1
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 2
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 3
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 4
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 5
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 6
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                }
                #endregion
            });
        }
    }

    public partial class Cleric{
        public override void initalizeSkillTree(){
            AddCards(new string[] {"Holy Strike", "Holy Strike", "Holy Strike", "Guard", "Guard", "Guard", "Restore", "Divinity", "Inspire", "Enfeeble"});
            AddToStarterPool(new string[] {"Restore", "Divinity", "Inspire", "Enfeeble"});
            skillTree = new StandardSkillTree(this, new Action[][][]{
                #region section 0
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Divine Healing", -1, null, this);
                                b.AddActivation(new AfterApplyHeal((heal, target)=>{
                                    target.addStatBuff("Regen", 1, 2, b.Self);
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                addStatBuff("Divine Vitality", "Regen", 1, -1, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                addUniqueLocal("Smite", 3, this);
                            });
                        }, ()=>{
                            MaxHealth += 10;
                            Health += 10;
                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{
                            AddToBeginnerPool(new string[]{"Urgent Defense", "Sip of Ichor", "Revitalize", "Healing Prayer", "Exalt"});
                        }, ()=>{
                            AddToBeginnerPool(new string[]{"Regenerative Plating", "Spiked Shield", "Divine Shield", "Enchanted Shield", "Perseverance"});
                        }, ()=>{
                            AddToBeginnerPool(new string[]{"Radiance", "Judgement", "Divine Protection", "Honor", "Twinsmite Strike"});
                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{
                            AddEncounterEffect(()=>{
                                List<Player> players = new List<Player>(Lobby.players.Values);
                                Player p = players[0];
                                for(int i = 1; i < players.Count; i++)
                                    if(players[i].Health < p.Health)
                                        p = players[i];
                                p.Heal(1, this);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Resolute", -1, null, this);
                                b.AddActivation(new AfterBuffApplied((buff)=>{
                                    if(buff.Caster == b.Self && buff.locals.ContainsKey("Shield")){
                                        b.Self.addStatBuff("Armor", 1, 1, b.Self);
                                        b.Self.addStatBuff("Resolve", 1, 1, b.Self);
                                    }
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                Buff b = new Buff("Radiant", -1, null, this);
                                b.AddActivation(new OnAttack((damage, target, Dtype, Atype)=>{
                                    if(Atype == AttackType.Smite)
                                        damage += 2;
                                    return damage;
                                }));
                                addBuff(b);
                            });
                        }, ()=>{
                            AddEncounterEffect(()=>{
                                List<Player> players = new List<Player>(Lobby.players.Values);
                                Player p = players[0];
                                for(int i = 1; i < players.Count; i++)
                                    if(players[i].Health < p.Health)
                                        p = players[i];
                                p.addStatBuff("Shield", 10, 2, this);
                            });
                        }
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{
                            AddCard("Redemption");
                        }, ()=>{
                            AddCard("Safeguard");
                        }, ()=>{
                            AddCard("Regicide");
                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 1
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 2
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 3
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 4
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 5
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 6
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                }
                #endregion
            });
        }
    }

    /* public partial class Template{
        public override void initalizeSkillTree(){
            skillTree = new StandardSkillTree(this, new Action[][][]{
                #region section 0
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{
                            
                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 1
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 2
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 3
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 4
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 5
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                },
                #endregion
                #region section 6
                new Action[][]{
                    #region row 0
                    new Action[]{ //row 0
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }
                    }, 
                    #endregion
                    #region row 1
                    new Action[]{ //row 1
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 2
                    new Action[]{ //row 2
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                        
                    }, 
                    #endregion
                    #region row 3
                    new Action[]{ //row 3
                        ()=>{

                        }, ()=>{

                        }, ()=>{

                        }                      
                    }
                    #endregion
                }
                #endregion
            });
        }
    }
    */
}