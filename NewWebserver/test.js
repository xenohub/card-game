
//logan, I know you're sped so I put this at the top so its easy for you to find
//change this variable to    t r u e    when you're testing it
//don't ask questions. No, im not lossing my mnid
//const usePublishedEXE = false;
//p.s. don't check the last few commits. trust me, its for your own protection

const express = require('express');
const stringify = require('json-stringify-safe');
const crypto = require('crypto');
const bodyParser = require('body-parser');
const requestIp = require('request-ip');
const cp = require('child_process');
var Convert = require('ansi-to-html');
var convert = new Convert();
const expressWs = require('express-ws');
const port = 80;
const path = require ('path');
require('fs').copyFileSync(path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/lobby.exe'), path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/win-x64/lobby.exe'));
require('fs').copyFileSync(path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/lobby.pdb'), path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/win-x64/lobby.pdb'));
require('fs').copyFileSync(path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/lobby.dll'), path.resolve(__dirname, 'lobby/bin/Debug/netcoreapp3.1/win-x64/lobby.dll'));
//COLOR CODING: CYAN-PROTOCOL BLUE-SUBPROTOCOL YELLOW-INCOMING GREEN-OUTGOING
function colorMarkup(dat){
	if(dat.indexOf(':') != -1) dat = '\u001b[96m'+dat.substring(0,dat.indexOf(':'))+'\u001b[0m'+dat.substring(dat.indexOf(':'));
	else dat = '\u001b[96m'+dat+'\u001b[0m';
	if(dat.substring(0,dat.indexOf(':')).includes('update')){
		var index = 0;
		while(true){
			if(dat.substring(index).indexOf(':') == -1) break;
			index += dat.substring(index).indexOf(':');
			dat = dat.substring(0,index)+'\u001b[94m'+dat.substring(index, dat.indexOf('!',index))+'\u001b[0m'+dat.substring(dat.indexOf('!',index));
			index += 7;
		}
	}
	return dat;
}

var app = express();
var wsInstance = expressWs(app);
app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.text({type:'text/plain'}));
app.use(function(req, res, next) { 
	req.ip = requestIp.getClientIp(req);
	req.cookies = {};
	if(req.headers.cookie !== undefined){
		var match = req.headers.cookie.match(/userID=[\w]{0,10}/g);
		if(match !== null){ 
			req.cookies.userID = match[0].substring(7);
			res.cookie('userID', req.cookies.userID);
		}
	}
	//uri encoded strings will contain -_.!~*'()% and a-z A-Z 0-9 
	if(req.body !== null) req.body = encodeURIComponent(req.body);
	for(var i in req.query){req.query[i] = encodeURIComponent(req.query[i]);}


					//QUERY AND BODY IS ENCODED


	//fs.writeFile('test.json', stringify(req, null, '\t'), (err)=>{if(err) console.error(err);});
	next();
});

var sockets = {};
var backdoors = {};

var usePublishedEXE = require('child_process').spawnSync('dotnet build', {cwd: "lobby/", shell:true}).stderr.toString() != "";
var server;
if(usePublishedEXE) server = cp.spawn('lobby.exe', {cwd: "lobby/bin/Debug/netcoreapp3.1/win-x64/", shell:true, stdio:'pipe'});
else server = cp.spawn('dotnet run', {cwd: 'lobby/', shell:true, stdio:'pipe'});
server.stderr.on('data', (chunk)=>{
	console.error('lobby error:\n' + chunk.toString());
	for(var ws in backdoors)
		backdoors[ws].send('lobby error:\n' + chunk.toString());
});
var msgqueue = '';
server.stdout.on('data', (chunk)=>{
	//console.log('\u001b[95mSERVER:\u001b[0m'+encodeURIComponent(chunk.toString()));
	msgqueue+=chunk.toString();
	msgcheck();
});
server.on('exit', (code, signal) => {
	console.error('the lobby has closed');
	for(var ws in backdoors)
		backdoors[ws].send('the lobby has closed');
	if(code) console.error('code: '+code);
	if(signal) console.error('signal: '+signal);
});
function msgcheck(){
	while(msgqueue.includes(';')){
		msgeval(msgqueue.substring(0, msgqueue.indexOf(';')));
		msgqueue = msgqueue.substring(msgqueue.indexOf(';')+1);
	}
}
function msgeval(input){
	if(input.substring(0,3) == '!~~'){
		var arr2 = input.substring(3).split(':');
		switch(arr2[0]){
			case 'to':
			var id = arr2[1];
			arr2.splice(0, 2);
			sockets[id].send(arr2.join(':')+';');
			var log = "\u001b[92mTO "+id+":\u001b[0m "+colorMarkup(arr2.join(':'));
			console.log(log);
			for(var ws in backdoors)
				backdoors[ws].send('-'+convert.toHtml(log));
			break;
			case 'kick':
			sockets[arr2[1]].close();
			delete sockets[arr2[1]];
			break;
		}
	}
	else if(input.substring(0,2) == '!~'){
		var log = "\u001b[92mSERVER:\u001b[0m "+colorMarkup(input.substring(2));
		console.log(log);
		for(var ws in backdoors)
			backdoors[ws].send('-'+convert.toHtml(log));
		for(var i in sockets){
			sockets[i].send(input.substring(2)+';');
		}
	}
	else if(input.substring(0,1) == '!'){
		var matches = input.match(/^!(\w+) ([\s\S]+)/);
		for(var ws in backdoors)
			if(ws == matches[1])
				backdoors[ws].send(matches[2]);
	}
	else {
		var log = "\u001b[95mDEBUG:\u001b[0m "+input;
		console.log(log);
		for(var ws in backdoors)
			backdoors[ws].send('-'+convert.toHtml(log));
	}
}

app.ws('/ws', (ws, req) =>{
	console.log('\u001b[95mwebsocket connected\u001b[0m');
	try{
		if(sockets[req.cookies.userID] == undefined){
			var id = req.cookies.userID;
			sockets[id] = ws;
			ws.on('message', function(msg) {
				if(msg == "dispose") return;
				var log = "\u001b[93m"+id+":\u001b[0m "+colorMarkup(msg);
				console.log(log);
				for(var ws in backdoors)
					backdoors[ws].send('-'+convert.toHtml(log));
				var arr = msg.split(':');
				for(var i in arr){
					arr[i] = encodeURIComponent(arr[i]);
				}
				msg = arr.join(':');
				server.stdin.write(id + ':' + msg.toString() + ';');
			});
			ws.on('close', function() {
				delete sockets[id];
                server.stdin.write(`system:userleft:${id};`);
			});
			server.stdin.write(`system:userjoined:${id};`);
		}
		else ws.close();
	} catch(err){console.error(err);}
});

var backdoor = {
	d3sync:"096df6ad7b2ef692ffcee69ce3e3d695f0f282112d1021d459b7e30a3cd9be03",
	cryoflame:"a963783e77f407e679ac96ace9a0fe647bf365f44d1968e57b8eb07b87e83113"
}
var backdoors = {};
app.get('/backdoor', function(req, res){
	res.render('backdoor'); 
});
app.ws('/backdoor', (ws, req)=>{
	var authenticated = false;
	var username = "";
	ws.send("Enter Username");
	ws.on('message', (msg)=>{
		if(msg == "dispose" || authenticated) return;
		if(username == ""){
			if(backdoor[msg.toLowerCase()] == undefined){
				ws.send("Invalid username");
			}
			else{
				username = msg;
				ws.send("Enter Password");
			}
		}
		else {
			var hash = crypto.createHash('sha256');
			hash.update(msg);
			var pass = hash.digest('hex');

			if(backdoor[username.toLowerCase()] != pass){
				ws.send("Invalid password");
			}
			else{
				ws.send("Backdoor unlocked. Executive commands authorized");
				initialize();
			}
		}
		
	});

	function initialize(){
		authenticated = true;
		console.log('\u001b[95muser '+username+' connected to backdoor\u001b[0m');
		backdoors[username] = ws;
		ws.on('message', function(msg) {
			if(msg == "dispose") return;
			console.log("\u001b[95m"+username+":\u001b[0m "+colorMarkup(msg));
			var cmd = msg.match(/^(\w+)/);
			if(!cmd) {
				ws.send("Invalid commmand");
				return;
			}
			var args = msg;
			try{
				arr = [];
				while(true){
					if(args.substring(0, 1) == '"'){
						var index = args.substring(1).indexOf('"')+1;
						if(index == 0) throw 'Invalid command';
						arr.push(args.substring(1, index));
						args = args.substring(index+1);
					}
					else {
						var index = args.indexOf(' ');
						if(index == -1){
							arr.push(args);
							break;
						}
						else{
							arr.push(args.substring(0, index));
							args = args.substring(index+1);
						}
					}
				}
				server.stdin.write(`system:backdoor:${username}:${arr.join(':')};`);
			}
			catch(err){
				ws.send("Invalid commmand");
			}
		});
		ws.on('close', function() {
			delete backdoors[backdoors.indexOf(ws)];
			console.log('\u001b[95muser '+username+' disconnected from backdoor\u001b[0m');
		});
	}
	
});

app.listen(port, ()=>{
	console.log('\u001b[95mTest websocket server operational, waiting for lobby to load...\u001b[0m');
}); 